import inputFinder_dvj as finder
import simpleHelperFncs as helper


import ROOT

import multiprocessing
import subprocess
import os

def scanMCForBroken(config, grid = 'GG_qqn1-rpvLF'):
    print("scanMCForBroken")
    json_file = os.path.join(os.getenv('ANA_FW_PATH'), config.GetValue("mc_files_per_rn_file", "").format(grid))
    dsid_file_dict = finder.mcFilesPerPeriod(json_file)

    path = config.GetValue("current_output_path", ".")

    count_total = 0
    count_broken = 0

    cmds = []

    for dsid_key in dsid_file_dict:
        for mc_priod in ["mc16a", "mc16d", "mc16e"]:
            foldername = os.path.join(path, mc_priod)
            filename = 'output.{}.{}.root'.format(mc_priod, dsid_key)
            fullname = os.path.join(foldername, filename)
            if not os.path.exists(fullname):
                print("WARNING: file does not exist: ", fullname)
                count_broken += 1
                cmd = "root -l -q -b \'histoMaker.cxx(\"{path}/{period}/metadata.{period}.{runumber}.cfg\")\' | tee {path}/{period}/logfile.{period}.{runumber}.resub.log".format(path=path, period=mc_priod, runumber=dsid_key)

                cmds.append(cmd)

            count_total += 1

    print("In total", count_broken, "MC files are missing out of", count_total)


def scanDataForBroken(config):
    print("scanDataForBroken")
    # json_file = os.path.join(os.getenv('ANA_FW_PATH'), config.GetValue("data_files_per_rn_file", ""))
    json_file = os.path.join(os.getenv('ANA_FW_PATH'), config.GetValue("data_files_per_rn_file", ""))
    json_file = json_file.format(config.GetValue("data_version", ""))
    files_dict = finder.dataFilesPerPeriod(json_file)

    path = config.GetValue("current_output_path", ".")

    count_total = 0
    count_broken = 0

    cmds = []

    for data_period in files_dict:
        if data_period == "data1516":
            continue

        foldername = os.path.join(path, data_period)

        for runnumber in files_dict[data_period]:
            # print("runnumber", runnumber)
            filename = 'output.{}.{}.root'.format(data_period, runnumber)
            fullname = os.path.join(foldername, filename)
            if not os.path.exists(fullname):
                print("WARNING: file does not exist: ", fullname)
                count_broken += 1
                cmd = "root -l -q -b \'histoMaker.cxx(\"{path}/{period}/metadata.{period}.{runumber}.cfg\")\' | tee {path}/{period}/logfile.{period}.{runumber}.resub.log".format(path=path, period=data_period, runumber=runnumber)

                cmds.append(cmd)

            count_total += 1

    print("In total", count_broken, "DATA files are missing out of", count_total)

    for cmd in cmds:
        # subprocess.call(cmd, shell=True))
        print(cmd)
    executeCommandsMT(cmds)

def execute(cmd, sema):
    try:
        with subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True) as p:
            out, err = p.communicate()
        sema.release()
    except:
        sema.release()

def executeCommandsMT(commands):
    from multiprocessing import Process
    from multiprocessing import Semaphore
    concurrency = 20
    sema = Semaphore(concurrency)
    all_processes = []

    for command in commands:
        sema.acquire()
        p = Process(target=execute, args=(command, sema))
        all_processes.append(p)
        p.start()

    for p in all_processes:
        p.join()


def main():
    print("Starting resubmitBrokenJobs")

    print("Read top config")

    top_config_filename = "topConfig.cfg"

    config = ROOT.TEnv(top_config_filename)

    scanDataForBroken(config)
    # scanMCForBroken(config)

if __name__ == "__main__":
    main()
