#!/bin/bash
setupATLAS

## This code pulls in two external packages:

## 1) the "plotting scripts" repo
## https://gitlab.cern.ch/atlas-phys-susy-wg/RPVLL/DisplacedVertices/dv-post-processing/plottingscripts

# export PYTHONPATH=$PYTHONPATH:/home/aleopold/jet_plus_dv/plottingscripts:`pwd`/python/
export PYTHONPATH=$PYTHONPATH:/afs/cern.ch/work/a/aleopold/private/jetdv/plottingscripts:`pwd`/python/


## 2) my private collection of "useful" code snippets
## https://gitlab.cern.ch/aleopold/aleopold-private-lib

# ALEX_LIB=/home/aleopold/software/aleopold-private-lib
ALEX_LIB=/afs/cern.ch/work/a/aleopold/private/software/personal_lib
export LD_LIBRARY_PATH=${ALEX_LIB}/install/shared/lib/:${LD_LIBRARY_PATH}
export ROOT_INCLUDE_PATH=${ALEX_LIB}/inc:$ROOT_INCLUDE_PATH

#############
## make code in the src folder visible for ROOT
export ROOT_INCLUDE_PATH=`pwd`/src:$ROOT_INCLUDE_PATH

# lsetup "root 6.24.06-x86_64-centos7-gcc8-opt"
lsetup "views LCG_101 x86_64-centos7-gcc8-opt"

## this environmental variable is used in many places to define paths relative
## to the root directory of this package
export ANA_FW_PATH=`pwd`

lsetup git
