import inputFinder_dvj as finder
import simpleHelperFncs as helper

import os
from datetime import datetime
from copy import deepcopy

from ROOT import TEnv

import multiprocessing
import subprocess


def createCfgFilesForData(topconfig):
    """
    The dict created by dataFilesPerPeriod looks like:
    {"dataX":
      {run1:["file1"], run2:["file2", "file3"]},
    "dataY":
      {run4:["file4", "file5"], run5:["file6", "file7"]}
    }
    """
    json_file = os.path.join(os.getenv('ANA_FW_PATH'), topconfig.GetValue("data_files_per_rn_file", ""))
    json_file = json_file.format(topconfig.GetValue("data_version", ""))
    print("loading json file", json_file, "for data config")
    files_dict = finder.dataFilesPerPeriod(json_file)

    for data_period in files_dict:
        for runnumber in files_dict[data_period]:
            filename = 'metadata.{}.{}.cfg'.format(data_period, runnumber)
            topconfig.SetValue('is_data', True)
            topconfig.SetValue('data_period', data_period)
            topconfig.SetValue('run_number', runnumber)
            topconfig.SetValue('input_files', helper.intListToString(files_dict[data_period][runnumber]))
            topconfig.SetValue('data_period', data_period)
            topconfig.SetValue('output_filename', 'output.{}.{}.root'.format(data_period, runnumber))

            output_config_path = os.path.join(topconfig.GetValue("current_output_path", ""), data_period)
            output_config_file = os.path.join(output_config_path, filename)
            topconfig.WriteFile(output_config_file)



def createCfgFilePerDSID(topconfig, grid = 'GG_qqn1-rpvLF'):
    # dsid_file_dict = finder.createGridMCDictionary(grid)

    json_file = os.path.join(os.getenv('ANA_FW_PATH'), topconfig.GetValue("mc_files_per_rn_file", "").format(grid))
    # print("json_file", json_file)
    dsid_file_dict = finder.mcFilesPerPeriod(json_file)
    # print(dsid_file_dict)
    # print(dsid_file_dict["448044"])
    # print(dsid_file_dict["448044"]['284500'])

    lumi_dict = finder.lumiDict()

    for dsid_key in dsid_file_dict:
        for mc_priod in ["mc", "mc16a", "mc16d", "mc16e"]:
        # for mc_priod in ["mc16a", "mc16d", "mc16e"]:
        # for mc_priod in ["mc16e"]:
            int_dsid = int(dsid_key)
            filename = 'metadata.{}.{}.cfg'.format(mc_priod, dsid_key)
            topconfig.SetValue('DSID', dsid_key)
            topconfig.SetValue('is_data', False)
            topconfig.SetValue('mc_period', mc_priod)
            # print('input_files', dsid_file_dict[dsid_key])
            # print('dsid_file_dict.values()', dsid_file_dict.values())
            run_number = finder.runNumberDict()[mc_priod]
            run_number_str = str(run_number[0])
            # print("select", dsid_key, run_number_str)
            input_files = dsid_file_dict[dsid_key][run_number_str]
            input_files_str = helper.listToString(input_files, ' ')
            topconfig.SetValue('input_files', input_files_str)
            topconfig.SetValue('lumi', float(lumi_dict[mc_priod]))
            topconfig.SetValue('run_numbers', helper.intListToString(run_number, ' '))
            topconfig.SetValue('cross_section', finder.crossSection(int_dsid))
            topconfig.SetValue('cross_section_unc', finder.getInfo(int_dsid, 'xsecUnc'))
            ## two signal regions, 'HighPtSR' or 'TracklessSR'
            topconfig.SetValue('campaign_weight_HighPtSR', finder.campaignWeight('HighPtSR'))
            topconfig.SetValue('sum_of_weights_HighPtSR', finder.sumOfWeights(int_dsid, 'HighPtSR'))
            topconfig.SetValue('campaign_weight_TracklessSR', finder.campaignWeight('TracklessSR'))
            topconfig.SetValue('sum_of_weights_TracklessSR', finder.sumOfWeights(int_dsid, 'TracklessSR'))
            topconfig.SetValue('output_filename', 'output.{}.{}.root'.format(mc_priod, int_dsid))
            topconfig.SetValue('legend', finder.getInfo(int_dsid, 'legend'))
            topconfig.SetValue('legendShort', finder.getInfo(int_dsid, 'legendShort'))
            topconfig.SetValue('grid', finder.getInfo(int_dsid, 'grid'))
            topconfig.SetValue('subgrid', finder.getInfo(int_dsid, 'subgrid'))

            output_config_path = os.path.join(topconfig.GetValue("current_output_path", ""), mc_priod)
            output_config_file = os.path.join(output_config_path, filename)
            topconfig.WriteFile(output_config_file)

def createFolderStructure(basepath, tag, make_dir = False):
    output_base_folder = os.path.join(basepath, 'output_{}'.format(tag))
    os.mkdir(output_base_folder)
    subfolders = ["data15", "data16", "data1516", "data17", "data18", "mc16a", "mc16d", "mc16e", "data", "mc", "plot_tmp"]
    for sf in subfolders:
        subpath = os.path.join(output_base_folder, sf)
        if make_dir:
            os.mkdir(subpath)
    return output_base_folder

def executeHistoCreationMC(path, period, dsid, sema):
    # cmd = "root -l -q -b \'mc_histoMaker.cxx(\"{path}/{period}/metadata.{period}.{dsid}.cfg\")\' | tee {path}/{period}/logfile.{period}.{dsid}.log".format(path=path, period=period, dsid=dsid)
    cmd = "root -l -q -b \'histoMaker.cxx(\"{path}/{period}/metadata.{period}.{dsid}.cfg\")\' | tee {path}/{period}/logfile.{period}.{dsid}.log".format(path=path, period=period, dsid=dsid)
    # print(cmd)
    with subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True) as p:
        out, err = p.communicate()
    print('release the Semaphore of', dsid)
    sema.release()


def submitHistCreationMCForOneGrid(config, grid):
    from multiprocessing import Process
    from multiprocessing import Semaphore

    # periods = ["mc16a", "mc16d", "mc16e", "mc"] ##prefer to creat 'mc' by hadd
    periods = ["mc16a", "mc16d", "mc16e"]
    # periods = ["mc16e"]
    dsid_file_dict = finder.createGridMCDictionary(grid)

    concurrency = 20
    sema = Semaphore(concurrency)
    all_processes = []

    output_base_folder = config.GetValue("current_output_path", ".")

    for period in periods:
        for dsid_key in dsid_file_dict:
            print("submitting:", period, dsid_key)
            sema.acquire()
            # executeHistoCreationMC(output_base_folder, period, dsid_key, sema)
            p = Process(target=executeHistoCreationMC, args=(output_base_folder, period, dsid_key, sema))
            all_processes.append(p)
            p.start()

    for p in all_processes:
        p.join()

def executeHistoCreationData(path, period, runumber, sema):
    cmd = "root -l -q -b \'histoMaker.cxx(\"{path}/{period}/metadata.{period}.{runumber}.cfg\")\' | tee {path}/{period}/logfile.{period}.{runumber}.log".format(path=path, period=period, runumber=runumber)
    # print(cmd)
    print('run:', runumber, ' with pid: ', os.getpid())
    ########
    # subprocess.call(cmd, shell=True)
    # sema.release()
    ########
    try:
        with subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True) as p:
            out, err = p.communicate()
        print('release the Semaphore of', runumber)
        sema.release()
    except:
        print("ERROR: crash in ", period, runumber)
        sema.release()

def submitHistCreationData(config):
    from multiprocessing import Process
    from multiprocessing import Semaphore

    # periods = ["mc16a", "mc16d", "mc16e", "mc"]
    json_file = os.path.join(os.getenv('ANA_FW_PATH'), topconfig.GetValue("data_files_per_rn_file", ""))
    json_file = json_file.format(topconfig.GetValue("data_version", ""))
    files_dict = finder.dataFilesPerPeriod(json_file)

    concurrency = 10
    sema = Semaphore(concurrency)
    all_processes = []

    output_base_folder = config.GetValue("current_output_path", ".")

    print('submitHistCreationData: parent process:', os.getppid())

    for period in files_dict:
        for runnumber in files_dict[period]:
            # executeHistoCreationData(output_base_folder, period, runnumber, sema)
            print("submitting:", period, runnumber)
            print("sema value:", sema.get_value())
            sema.acquire(timeout=120)
            p = Process(target=executeHistoCreationData, args=(output_base_folder, period, runnumber, sema))
            all_processes.append(p)
            p.start()

    for p in all_processes:
        p.join()

def writeCondorExecuteFile():

    cmd = """

    """
    pass

def submitMCToCondor(config, grid = 'GG_qqn1-rpvLF'):
    ## setup folders
    subprocess.call('mkdir -p LOG_MC', shell=True)
    subprocess.call('mkdir -p LINKS_MC', shell=True)
    subprocess.call('rm LINKS_MC/*.cfg', shell=True)
    subprocess.call('rm LOG_MC/*.log', shell=True)

    output_base_folder = config.GetValue("current_output_path", ".")

    json_file = os.path.join(os.getenv('ANA_FW_PATH'), config.GetValue("mc_files_per_rn_file", ""))
    json_file = json_file.format(grid)
    files_dict = finder.dataFilesPerPeriod(json_file)
    # files_dict = finder.dataFilesPerPeriod(config.GetValue("data_files_per_rn_file", ""))

    periods = ["mc16a", "mc16d", "mc16e"]
    # periods = ["mc16e"]
    dsid_file_dict = finder.createGridMCDictionary(grid)

    itr = 0
    for period in periods:
        for dsid_key in dsid_file_dict:
            # print("submitting:", period, dsid_key)
            cfg_file = "{path}/{period}/metadata.{period}.{dsid}.cfg".format(path=output_base_folder, period=period, dsid=dsid_key)
            cfg_link = 'LINKS_MC/metadata.{itr}.cfg'.format(itr=itr)
            link_cmd = "ln -s {file} {link}".format(file=cfg_file, link=cfg_link)
            subprocess.call(link_cmd, shell=True)
            itr += 1

    condor_submission_form = """
executable              = executeCondorJob.sh
arguments               = LINKS_MC/metadata.$(ProcId).cfg
requirements            = (OpSysAndVer =?= "CentOS7")
output                  = LOG_MC/logfile$(ProcId).log
error                   = LOG_MC/logfile$(ProcId).log
log                     = LOG_MC/logfile$(ProcId).log
+JobFlavour             = "microcentury"
queue {n_jobs}
""".format(n_jobs=itr)

    with open("condorsubmissionMC.sub", "wt") as cond_sub:
        cond_sub.write(condor_submission_form)

def submitDataToCondor(config):
    ## setup folders
    subprocess.call('mkdir -p LOG_DATA', shell=True)
    subprocess.call('mkdir -p LINKS_DATA', shell=True)
    subprocess.call('rm LINKS_DATA/*.cfg', shell=True)
    subprocess.call('rm LOG_DATA/*.log', shell=True)

    output_base_folder = config.GetValue("current_output_path", ".")

    json_file = os.path.join(os.getenv('ANA_FW_PATH'), config.GetValue("data_files_per_rn_file", ""))
    json_file = json_file.format(config.GetValue("data_version", ""))
    files_dict = finder.dataFilesPerPeriod(json_file)
    # files_dict = finder.dataFilesPerPeriod(config.GetValue("data_files_per_rn_file", ""))

    itr = 0
    for period in files_dict:
        # if period != '2017':
        #     continue
        for runnumber in files_dict[period]:
            # if no runnumber in ['335016', '335022', '335056', '335082', '335083', '335131', '335170', '335177', '335222', '335282', '335290', '336506', '336548', '336567', '336630']:
            #     continue
            # print("submitting:", period, runnumber)
            cfg_file = "{path}/{period}/metadata.{period}.{runnumber}.cfg".format(path=output_base_folder, period=period, runnumber=runnumber)
            cfg_link = 'LINKS_DATA/metadata.{itr}.cfg'.format(itr=itr)
            link_cmd = "ln -s {file} {link}".format(file=cfg_file, link=cfg_link)
            subprocess.call(link_cmd, shell=True)
            itr += 1

    condor_submission_form = """
executable              = executeCondorJob.sh
arguments               = LINKS_DATA/metadata.$(ProcId).cfg
requirements            = (OpSysAndVer =?= "CentOS7")
output                  = LOG_DATA/logfile$(ProcId).log
error                   = LOG_DATA/logfile$(ProcId).log
log                     = LOG_DATA/logfile$(ProcId).log
+JobFlavour             = "microcentury"
queue {n_jobs}
""".format(n_jobs=itr)

    with open("condorsubmissionDATA.sub", "wt") as cond_sub:
        cond_sub.write(condor_submission_form)


def main():
    """
    histograms for data and mc and for each period
    folder strucure:
    output_<sometag>
     - data1516
        - metadata.data1516.cfg
        - output.data1516.root
     - data17
     - data18
     - mc16a
        - metadata.mc16a.DSID.cfg
        - output.DSID.root
     - mc16d
     - mc16e
    create an output rootfile that stores the same historgram (...) structure
    for every input sample (data&mc)
    """

    print("Starting createHistos")

    print("Read top config")

    top_config_filename = "topConfig.cfg"

    config = TEnv(top_config_filename)
    output_path_base = config.GetValue("output_hist_path", "~")

    time_tag = datetime.now().strftime("%Y%m%d_%H%M%S")

    if (config.GetValue("current_output_path", ".") == ""):
        print("Create folder structure")
        output_base_folder = createFolderStructure(output_path_base, time_tag, True)
        config.SetValue("current_output_path", output_base_folder)
        print("Creating new folder structure for path: ", output_base_folder)
    # else:
    #     output_base_folder = config.GetValue("current_output_path", ".")


    if (config.GetValue("create_mc_config_files", False)):
        print("Create MC config files")
        createCfgFilePerDSID(deepcopy(config), 'GG_qqn1-rpvLF')

    if (config.GetValue("create_data_config_files", False)):
        print("Create DATA config files")
        createCfgFilesForData(deepcopy(config))

    if (config.GetValue("submit_mc_jobs", False)):
        print("Submit MC jobs")
        submitHistCreationMCForOneGrid(config, 'GG_qqn1-rpvLF')

    if (config.GetValue("submit_data_jobs", False)):
        print("Submit DATA jobs")
        submitHistCreationData(config)

    if (config.GetValue("submit_data_condor_jobs", False)):
        print("Prepare DATA config file links for condor job submission")
        submitDataToCondor(config)

    if (config.GetValue("submit_mc_condor_jobs", False)):
        print("Prepare MC config file links for condor job submission")
        submitMCToCondor(config, 'GG_qqn1-rpvLF')


    return

if __name__ == "__main__":
    main()
