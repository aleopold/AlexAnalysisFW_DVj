import ROOT
import inputFinder_dvj as finder

def plot1D(object_name, file, path):
    ROOT.gROOT.SetStyle('ATLAS')

    canvas = ROOT.TCanvas()

    plotname = "{}/{}.pdf".forma(path, object_name)

    hist = file.Get(object_name)

    hist.Draw()

    label = ROOT.TLatex()
    label.SetNDC()
    label.SetTextFont(42)
    label.SetTextAlign(13)
    label.SetTextSize(0.045)
    label.DrawLatex(0.175, 0.925, "#font[72]{ATLAS %s}, #font[52]{#sqrt{s}} = 13 TeV, %.0f fb^{ -1}".format("Internal", meta.lumiPerSR[args.sr]*1e-3))

    canvas.Print(plotname)
    plotname = plotname.replace(".pdf", ".png")
    canvas.Print(plotname)


def plot2D(input_file, object_name, file, output_path):
    ROOT.gROOT.SetStyle('ATLAS')

    canvas = ROOT.TCanvas()

    plotname = "{}/{}.pdf".format(path, object_name)

    hist2D = file.Get(object_name)

    hist.Draw()

    label = ROOT.TLatex()
    label.SetNDC()
    label.SetTextFont(42)
    label.SetTextAlign(13)
    label.SetTextSize(0.045)
    label.DrawLatex(0.175, 0.925, "#font[72]{ATLAS %s}, #font[52]{#sqrt{s}} = 13 TeV, %.0f fb^{ -1}".format("Internal", meta.lumiPerSR[args.sr]*1e-3))

    canvas.Print(plotname)
    plotname = plotname.replace(".pdf", ".png")
    canvas.Print(plotname)

if __name__ == "__main__":
    main()
