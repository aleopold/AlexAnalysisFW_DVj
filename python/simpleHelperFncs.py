import json

def getFilesContainingAnySubstring(files, tags):
    return [file for file in files if any(tag in file for tag in tags)]

def readListFromFile(path):
    with open(path) as f:
        list = [x.rstrip() for x in f]
    return list

def listToString(list, delim=' '):
    str = ''
    for el in list:
        str += el + delim
    return str

def loadJsonFile(filename):
    with open(filename) as json_file:
        data = json.load(json_file)
    return data

def callCmd(cmd, id, sema):
    import subprocess
    try:
        with subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True) as p:
            out, err = p.communicate()
        print('release the Semaphore of', id)
        sema.release()
    except:
        print("ERROR: crash in ", id)
        sema.release()
        

def intListToString(list, delim=' '):
    # str = ''
    # print(list)
    # for el in list:
    #     print(el)
    #     str += str(int(el)) + delim
    # return str
    converted_list = [str(element) for element in list]
    joined_string = " ".join(converted_list)
    return joined_string
