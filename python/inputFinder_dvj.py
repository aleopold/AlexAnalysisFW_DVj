import meta
import sampleinfo

import glob
import os

import simpleHelperFncs as helper

# amiTagsForLumiPeriod = {
#     "data" : ["r10740", "r11036", "r11008", "r11802", "r11915", "r10739", "r11037", "r11803", "r11916", "r10790", "r11038", "r11265", "r11815", "r11891"],
#     "data1516": ["r10740", "r11036", "r11008", "r11802", "r11915"], # mc16a
#     "data17": ["r10739", "r11037", "r11803", "r11916"], # mc16d
#     "data18": ["r10790", "r11038", "r11265", "r11815", "r11891"] # mc16e
# }

def getDSIDFromParams(param_dict = {'mGluino': 2200, 'mChi0': 2050, 'dMass': 150, 'tau': 0.1}):
    """
    example of what we work with here:
    print(sampleinfo.sampleDict[448149])
{'mGluino': 2200, 'mChi0': 2050, 'dMass': 150, 'tau': 0.1, 'xsec': 0.000356, 'xsecUnc': 7.77e-05, 'grid': 'GG_qqn1', 'subgrid': 'rpvLF', 'legend': '#splitline{Strong RPV #font[152]{#tilde{c}}^{0} LF, #font[152]{t} = 0.1 ns}{#font[52]{m(#tilde{g})} = 2200, #font[52]{m(#font[152]{#tilde{c}}^{0})} = 2050}', 'legendShort': 'LF, #font[152]{t} = 0.1 ns, #font[52]{m(#tilde{g})} = 2200, #font[52]{m(#font[152]{#tilde{c}}^{0})} = 2050', 'sumW_ade': 10030.442655672687, 'sumW_de': 10029.510553393196, 'sumW_2018Kplus': 10029.901618123055}
    """
    for dsid in sampleinfo.sampleDict:
        ## check if all the param values are the same
        if set(param_dict.items()).issubset(sampleinfo.sampleDict[dsid].items()):
            return dsid

    ## if none was found, return None!
    return None




def dataRunNumberBoundaries():
    dict = {
      "data1516" : {
        "runNumberMinMax" : [266904, 311481]
      },
      "data15" : {
        "runNumberMinMax" : [266904, 284484]
        ## https://atlas-groupdata.web.cern.ch/atlas-groupdata/GoodRunsLists/data15_13TeV/20170619/
        ## 276262 - 284484
      },
      "data16" : {
        "runNumberMinMax" : [297730, 311481]
        ## https://atlas-groupdata.web.cern.ch/atlas-groupdata/GoodRunsLists/data16_13TeV/20180129/
        ## 297730 - 311481
      },
      "data17" : {
        "runNumberMinMax" : [324320, 341649]
        ## https://atlas-groupdata.web.cern.ch/atlas-groupdata/GoodRunsLists/data17_13TeV/20180619/
        ## 325713 - 340453
      },
      "data18" : {
        "runNumberMinMax" : [348197, 364485]
        ## https://atlas-groupdata.web.cern.ch/atlas-groupdata/GoodRunsLists/data18_13TeV/20190318/
        ## says instead: 348885 - 364292
      }
    }
    return dict

def treeName():
    return meta.TREENAME

# def getAMITagOfDataPeriod(period):
    # return amiTagsForLumiPeriod[period]

def lumi1516():
    return meta.lumi1516

def lumi17():
    return meta.lumi17

def lumi18():
    return meta.lumi18

def totalLumi():
    return meta.LUMI

def lumiDict():
    return {'mc':totalLumi(), 'mc16a':lumi1516(), 'mc16d':lumi17(), 'mc16e':lumi18()}


def runNumberDict():
    return {'mc':[284500, 300000, 310000], 'mc16a':[284500], 'mc16d':[300000], 'mc16e':[310000]}

def dataFilesPerPeriod(json_file_name):
    boundaries = dataRunNumberBoundaries()
    runnumber_files = helper.loadJsonFile(json_file_name)
    newdict = {}
    for period in boundaries:
        min = boundaries[period]['runNumberMinMax'][0]
        max = boundaries[period]['runNumberMinMax'][1]
        period_dict = {}
        for runnumber in runnumber_files:
            if int(runnumber)>=min and int(runnumber)<=max:
                period_dict[int(runnumber)] = runnumber_files[runnumber]
        newdict[period] = period_dict
    return newdict

def mcFilesPerPeriod(json_file_name):
    runnumber_files = helper.loadJsonFile(json_file_name)
    return runnumber_files
    # boundaries = runNumberDict()
    # runnumber_files = helper.loadJsonFile(json_file_name)
    # newdict = {}
    # for period in boundaries:
    #     min = boundaries[period]['runNumberMinMax'][0]
    #     max = boundaries[period]['runNumberMinMax'][1]
    #     period_dict = {}
    #     for runnumber in runnumber_files:
    #         if int(runnumber)>=min and int(runnumber)<=max:
    #             period_dict[int(runnumber)] = runnumber_files[runnumber]
    #     newdict[period] = period_dict
    # return newdict


def treename():
    return meta.TREENAME

def crossSection(dsid):
    return sampleinfo.sampleDict[dsid]["xsec"]

def getInfo(dsid, info):
    return sampleinfo.sampleDict[dsid][info]

def campaignWeight(signal_region):
    return meta.cwStrPerSR[signal_region]

def sumOfWeights(dsid, signal_region):
    return sampleinfo.sampleDict[dsid][meta.sumStrPerSR[signal_region]]

def createDataDictionary():
    """
    creates a dictionary of all the data and mc, split into periods
    """
    return

def createFullMCDictionary():
    grids = sampleinfo.validUniqueGrids()
    output_dict = {}
    for grid in grids:
        output_dict[grid] = createGridMCDictionary(grid)
    return output_dict


def createGridMCDictionary(gridname):
    """
    Returns a dictionary {DSID: /path/to/files.*.root}
    """
    dsids = sampleinfo.getDSIDs(gridname)
    output_dict = {}
    for dsid in dsids:
        fn = getInputNtupleFileListPerDSID(dsid)
        output_dict[dsid] = fn
    return output_dict

def getInputNtupleFileListPerDSID(dsid):
    return glob.glob(os.path.join(meta.DATAPATH, "group.phys-susy.{}.*.root".format(dsid)))

def getInputDataNtuplesFileList(config):
    """
    The input files are read from a text file and include the full Run2 data.
    """
    import ROOT
    # top_config_filename = os.path.join(os.getenv('ANA_FW_PATH'), "topConfig.cfg")
    # config = ROOT.TEnv(top_config_filename)
    textfile_path = config.GetValue("data_paths_file", "default")
    with open(textfile_path) as f:
        file_list = [x.rstrip() for x in f]
    return file_list

def getDSIDsforGrid(gridname):
    """
    Main grid for DV+jets is 'GG_qqn1-rpvLF'
    """
    return sampleinfo.getDSIDs(gridname)


if __name__ == "__main__":
    """
    For testing of the methods and includes
    """
    print(meta.DATAPATH)
    print(sampleinfo.validUniqueGrids())
    print(sampleinfo.validParameters('GG_qqn1-rpvLF'))
    print(sampleinfo.getDSIDs('GG_qqn1-rpvLF'))
    # print(sampleinfo.validUniqueGrids())
    print(getDSIDFromParams())
