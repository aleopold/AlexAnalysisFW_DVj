// clang-format off
/**
 * @file mc_histoMaker.cxx
 * @author Alexander Leopold (alexander.leopold@cern.ch)
 * @date Jan, 2022
 * @brief
 *
 * root -l -q -b 'histoMaker.cxx("topConfig.cfg")'
 * root -l -q -b 'histoMaker.cxx("/eos/user/a/aleopold/jetdv/outputs/output_20220301_113653/mc16e/metadata.mc16e.448114.cfg")'
 * root -l -q -b 'histoMaker.cxx("/eos/user/a/aleopold/jetdv/outputs/output_20220301_113653/data18/metadata.data18.356205.cfg")' &
 *
 */
// clang-format on

#include "CommonHelpers.h"
#include "Configurator.h"

#include "DefineHistograms.cxx"
#include "DefineVariablesAndRegions.cxx"
#include "HelperFunctions.cxx"

void histoMaker(TString config_path = "topConfig.cfg") {

  // ROOT::EnableImplicitMT();

  std::cout << "Welcome to mc_histoMaker\n";

  std::cout << "Reading the config: " << config_path << '\n';

  AL::Configurator config(config_path);

  bool is_data = config.getBool("is_data");

  TString period = "";
  TString run_number = "";
  if (is_data) {
    period = config.getStr("data_period");
    run_number = config.getStr("run_number");
  } else {
    period = config.getStr("mc_period");
    run_number = config.getStr("DSID");
  }

  std::cout << "Processeing perid: " << period << ", identifier: " << run_number
            << '\n';

  // std::vector<TString> files_list =
  //     AL::getListOfFilesNoCheck(config.getStr("data_paths_file"));
  //
  // cleanBadFiles(files_list);

  // TChain* chain = AL::getChain(files_list, "trees_SRDV_", 1);
  std::vector<TString> files_list = config.getStrV("input_files");

  int skip = files_list.at(files_list.size() - 1) == "" ? 1 : 0;

  std::cout << "Using files: \n";
  for (const auto& file : files_list) {
    std::cout << "  - " << file << '\n';
  }

  if (files_list.size() - skip <= 0) {
    std::cout << "ERROR: NO INPUT FILES FOUND \n";
    return -1;
  }

  std::cout << "Build TChain\n";
  TChain* chain = AL::getChain(
      std::vector<TString>(files_list.begin(), files_list.end() - skip),
      "trees_SRDV_", -1);

  std::cout << "Creating RDataFrame\n";
  ROOT::RDataFrame data_frame(*chain);

  // SET TO FALSE AFTER UNBLINDING!!
  bool blind_sr = is_data;

  float lumi = 0;
  float xsec = 0;
  float sum_of_weights = 0;
  // float sum_of_weights_trackless = 0;
  if (not is_data) {
    lumi = config.getNum("lumi");
    xsec = config.getNum("cross_section");
    // sum_of_weights = config.getNum("campaign_weight_HighPtSR");
    sum_of_weights = getSumOfWeights(
        std::vector<TString>(files_list.begin(), files_list.end() - skip));
    if (sum_of_weights < 0) {
      std::cout << "ERROR: SUM OF WEIGHTS NOT FOUND\n";
      return -1;
    }
    // sum_of_weights_trackless = config.getNum("sum_of_weights_TracklessSR");
    std::cout << "Lumi: " << lumi << " [pb-1] xs: " << xsec
              << " [pb],  SOW: " << sum_of_weights << '\n';
  }
  // for MC, global weight withoug MC weight is:
  float weight_wo_mcw = lumi * xsec / sum_of_weights;

  std::cout << "Define additional variables on the dataframe\n";

  auto extended_df =
      defineAdditionalVariables(data_frame, is_data, weight_wo_mcw, blind_sr);

  // add additional filters
  // for MC, scan only current RunNumber
  // std::string additional_filter = "";
  // if (not is_data) {
  //   TString tmp = Form("runNumber == %s", run_number.Data());
  //   additional_filter = tmp.Data()
  // }

  HistOutputStorage storage;

  // regions: HighPT, Trkless
  // selection: Pre, VR, SR
  /////////////////////  any  ////////////////////////////
  auto anysel_df = getPreselectionFrame(extended_df, "passes_any");

  scheduleH1s(anysel_df, storage, "any");
  scheduleH2s(anysel_df, storage, "any");

  /////////////  for dv occupancy plots  ////////////////////
  auto occ_df_highpt = getPreselectionFrame(extended_df, "passes_HighPT_OCCUP");

  scheduleH1s(occ_df_highpt, storage, "HighPT_OCCUP");
  scheduleH2s(occ_df_highpt, storage, "HighPT_OCCUP");
  scheduleEffs(extended_df, storage, "HighPT_OCCUP", blind_sr);

  auto occ_df_trkless =
      getPreselectionFrame(extended_df, "passes_Trkless_OCCUP");

  scheduleH1s(occ_df_trkless, storage, "Trkless_OCCUP");
  scheduleH2s(occ_df_trkless, storage, "Trkless_OCCUP");
  scheduleEffs(extended_df, storage, "Trkless_OCCUP", blind_sr);

  auto occ_df_highptnew = getPreselectionFrame(extended_df, "passes_HighPTNEW_OCCUP");

  scheduleH1s(occ_df_highptnew, storage, "HighPTNEW_OCCUP");
  scheduleH2s(occ_df_highptnew, storage, "HighPTNEW_OCCUP");
  scheduleEffs(extended_df, storage, "HighPTNEW_OCCUP", blind_sr);

  auto occ_df_trklessnew =
      getPreselectionFrame(extended_df, "passes_TrklessNEW_OCCUP");

  scheduleH1s(occ_df_trklessnew, storage, "TrklessNEW_OCCUP");
  scheduleH2s(occ_df_trklessnew, storage, "TrklessNEW_OCCUP");
  scheduleEffs(extended_df, storage, "TrklessNEW_OCCUP", blind_sr);

  /////////////////////  HighPT  ////////////////////////////
  std::cout << "Retrieve the selection frames and schedule the histograms\n";
  auto presel_highpt_df = getPreselectionFrame(extended_df, "passes_HighPTPre");

  scheduleH1s(presel_highpt_df, storage, "HighPTPre");
  scheduleH2s(presel_highpt_df, storage, "HighPTPre");
  scheduleEffs(extended_df, storage, "HighPTPre", blind_sr);

  auto presel_highpt_df_new =
      getPreselectionFrame(extended_df, "passes_HighPTPreNEW");

  scheduleH1s(presel_highpt_df_new, storage, "HighPTPreNEW");
  scheduleH2s(presel_highpt_df_new, storage, "HighPTPreNEW");
  scheduleEffs(extended_df, storage, "HighPTPreNEW", blind_sr);

  auto vr_highpt_frame =
      getValidationRegionFrame(extended_df, "passes_HighPTPre");

  scheduleH1s(vr_highpt_frame, storage, "HighPTVR");
  scheduleH2s(vr_highpt_frame, storage, "HighPTVR");
  scheduleEffs(extended_df, storage, "HighPTVR", blind_sr);

  auto sr_highpt_frame =
      getSignalRegionFrame(extended_df, "passes_HighPTPre", blind_sr);

  scheduleH1s(sr_highpt_frame, storage, "HighPTSR");
  scheduleH2s(sr_highpt_frame, storage, "HighPTSR");
  scheduleEffs(extended_df, storage, "HighPTSR", blind_sr);
  /////////////////////  Trkless  ////////////////////////////
  std::cout << "Retrieve the selection frames and schedule the histograms\n";
  auto presel_trkls_df = getPreselectionFrame(extended_df, "passes_TrklessPre");

  scheduleH1s(presel_trkls_df, storage, "TrklessPre");
  scheduleH2s(presel_trkls_df, storage, "TrklessPre");
  scheduleEffs(extended_df, storage, "TrklessPre", blind_sr);

  auto presel_trkls_df_new =
      getPreselectionFrame(extended_df, "passes_TrklessPreNEW");

  scheduleH1s(presel_trkls_df_new, storage, "TrklessPreNEW");
  scheduleH2s(presel_trkls_df_new, storage, "TrklessPreNEW");
  scheduleEffs(extended_df, storage, "TrklessPreNEW", blind_sr);

  auto vr_trkls_frame =
      getValidationRegionFrame(extended_df, "passes_TrklessPre");

  scheduleH1s(vr_trkls_frame, storage, "TrklessVR");
  scheduleH2s(vr_trkls_frame, storage, "TrklessVR");
  scheduleEffs(extended_df, storage, "TrklessVR", blind_sr);

  auto sr_trkls_frame =
      getSignalRegionFrame(extended_df, "passes_TrklessPre", blind_sr);

  scheduleH1s(sr_trkls_frame, storage, "TrklessSR");
  scheduleH2s(sr_trkls_frame, storage, "TrklessSR");
  scheduleEffs(extended_df, storage, "TrklessSR", blind_sr);
  /////////////////////////////////////////////////
  TString current_output_path = config.getStr("current_output_path");

  TString output_path =
      Form("%s/%s/", current_output_path.Data(), period.Data());
  output_path += config.getStr("output_filename");

  std::cout << "Write output file (eventloop is run here, please wait...)\n";
  storage.writeToFile(output_path.Data());

  std::cout << "Written to file: " << output_path.Data() << '\n';

  std::cout << "Done, bye!\n";
}
