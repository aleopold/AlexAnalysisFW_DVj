#!/bin/bash
cd /afs/cern.ch/work/a/aleopold/private/jetdv/AlexAnalysisFW_DVj

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

# setupATLAS


export PYTHONPATH=$PYTHONPATH:/afs/cern.ch/work/a/aleopold/private/jetdv/plottingscripts:`pwd`/python/

export ROOT_INCLUDE_PATH=`pwd`/src:$ROOT_INCLUDE_PATH

ALEX_LIB=/afs/cern.ch/work/a/aleopold/private/software/personal_lib
export LD_LIBRARY_PATH=${ALEX_LIB}/install/shared/lib/:${LD_LIBRARY_PATH}
export ROOT_INCLUDE_PATH=${ALEX_LIB}/inc:$ROOT_INCLUDE_PATH

# lsetup "root 6.24.06-x86_64-centos7-gcc8-opt"
lsetup "views LCG_101 x86_64-centos7-gcc8-opt"

export ANA_FW_PATH=`pwd`

config_file=$1

# root -l -q -b $cmd
root -l -q -b "histoMaker.cxx(\"$config_file\")"
