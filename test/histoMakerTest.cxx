#include "CommonHelpers.h"
#include "Configurator.h"
#include "HelperFunctions.h"

// void bookHisto1D(ROOT::RDF::RResultPtr<TH1D> hist) {
// g_histos.push_back(hist); }

std::vector<ROOT::RDF::RResultPtr<TH1D>> createDataHistograms(TChain* chain) {

  std::vector<ROOT::RDF::RResultPtr<TH1D>> histos;

  ROOT::RDataFrame full_data_frame(*chain);

  // auto selectPeriod = [](int run_number, int min_rn, int max_rn) {
  //   return run_number >= min_rn and run_number <= min_rn;
  // };
  //
  // auto data1516_df =
  //     full_data_frame.Filter(selectPeriod, {"runNumber", 266904, 311481});

  auto h_dv_r_nocuts = full_data_frame.Histo1D(
      {"h_dv_r_nocuts", ";#DV radius [mm];Entries", 100, 0, 30}, "DV_rxy");
  // h_dv_r_nocuts.OnPartialResultSlot(1000,
  //                                   []() { std::cout << "working...\n"; });
  histos.push_back(h_dv_r_nocuts);
  //
  // auto h_dv_r_baseling_sel = full_data_frame.Filer().Histo1D(
  //     {"h_dv_r_baseling_sel", ";#DV radius [mm];Entries", 100, 0, 30},
  //     "DV_rxy");
  // bookHisto1D(h_presel);

  return histos;
}

void writeDataHistos(AL::Configurator& config,
                     std::vector<ROOT::RDF::RResultPtr<TH1D>>& histos,
                     int part_i) {
  std::cout << " - Writing output " << part_i << " ...\n";
  TFile* output_file = TFile::Open(config.getStr("output_hist_path") +
                                       Form("/data.testfile.%i.root", part_i),
                                   "RECREATE");
  if (not output_file) {
    std::cout << "ERROR: no output file created\n";
    return;
  }

  // write all histograms
  for (auto& histo : histos) {
    histo->Write();
  }

  output_file->Close();
}

void doData(AL::Configurator& config) {
  std::cout << " - Reading the input files list\n";

  std::vector<TString> files_list =
      AL::getListOfFilesNoCheck(config.getStr("data_paths_file"));

  cleanBadFiles(files_list);

  // size_t files_list_size = files_list.size();
  // std::cout << "INFO: Found " << files_list_size << " input files.\n";
  //
  // int files_per_batch = 50;
  // int n_batches = files_list_size / files_per_batch + 1;
  //
  // // TODO SPLIT THIS UP INTO SUBPARTS, something doesn't run right!
  // std::cout << " - Constructing the TChain\n";
  // for (size_t begin = 0, end = files_per_batch, i = 0; i < n_batches;
  //      begin += files_per_batch, end += files_per_batch, i++) {
  //   if (end > files_list_size) {
  //     end = files_list_size;
  //   }
  //   auto sub_list = std::vector<TString>(files_list.begin() + begin,
  //                                        files_list.begin() + end);
  //
  //   if (containsBadFiles(sub_list)) {
  //     continue;
  //   }
  //   std::cout << "INFO: processing " << sub_list.size() << " input files.\n";
  //   TChain* chain = AL::getChain(sub_list, "trees_SRDV_", -1);
  //   std::cout << " - Starting work ...\n";
  //   auto histos = createDataHistograms(chain);
  //   writeDataHistos(config, histos, i);
  // }

  TChain* chain = AL::getChain(files_list, "trees_SRDV_", -1);

  std::cout << " - Starting work ...\n";
  auto histos = createDataHistograms(chain);
  writeDataHistos(config, histos, 0);
}

void histoMakerTest() {

  ROOT::EnableImplicitMT(30);

  //////////////////////////////////////////////////////
  /// SETUP

  std::cout << "Welcome to histoMaker\n";

  std::cout << "Reading the topConfig\n";
  AL::Configurator config(TString("topConfig.cfg"));

  //////////////////////////////////////////////////////
  /// WORK
  doData(config);

  //////////////////////////////////////////////////////
  // OUTPUT

  std::cout << "Done, bye!\n";
}
