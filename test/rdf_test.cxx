/**
 * @author Alexander Leopold (alexander.leopold@cern.ch)
 * @brief  Defines helper classes to book objects beyond THn
 */

#include "BookHelpers.cxx"
// using namespace ROOT;

void rdf_test() {

  std::cout << "Welcome to rdf_test\n";

  // ROOT::EnableImplicitMT();

  TString base_path = "/eos/atlas/atlascerngroupdisk/phys-susy/"
                      "DVjets_ANA-SUSY-2018-13/ntuples_210616/";

  TString mc_folder =
      "group.phys-susy.user.cohm.mc16_13TeV.245bc66d.210616_trees.root/";
  TString mc_file = "group.phys-susy.600831.e8352_e7400_s3338_r11891_p4302."
                    "25903063._001653.trees.root";

  TString data_folder =
      "group.phys-susy.user.cohm.dataXY_13TeV.245bc66d.210616_trees.root";
  // TString data_file = "group.phys-susy.00363400.r11969_r11784_p4072_p4296."
  //                     "25903127._000816.trees.root";
  TString data_file = "group.phys-susy.00363400.r11969_r11784_p4072_p4296."
                      "25903127.*.trees.root";

  // TString fullpath =
  //     Form("%s/%s/%s", base_path.Data(), mc_folder.Data(), mc_file.Data());

  TString fullpath =
      Form("%s/%s/%s", base_path.Data(), data_folder.Data(), data_file.Data());

  std::cout << "loading RDF...\n";
  ROOT::RDataFrame rdf("trees_SRDV_", fullpath.Data());

  auto presel_df = rdf.Filter("BaselineSel_HighPtSR==1");
  // auto dv_presel_df = rdf.Filter("BaselineSel_HighPtSR==1 &&
  // DV_passFiducialCut==1 && DV_passChiSqCut==1 && DV_passDistCut==1 &&
  // DV_passMaterialVeto_strict==1");

  using RVecB = ROOT::VecOps::RVec<int>;
  using RVecUL = ROOT::VecOps::RVec<unsigned long>;

  RVecB a = {1, 2, 3};
  RVecB b = {0, 0, 1};
  RVecB c = (a * 2)[b == 1];
  std::cout << "c has size: " << c.size() << '\n';

  // select good dvs
  auto good_dv_index_selector =
      [](const RVecB& DV_passChiSqCut, const RVecB& DV_passDistCut,
         const RVecB& DV_passMaterialVeto_strict, const RVecUL& DV_index) {
        // RVecUL indices;
        // for (size_t i = 0; i < DV_index.size(); i++) {
        //   if (DV_passChiSqCut.at(i) and DV_passDistCut.at(i) and
        //       DV_passMaterialVeto_strict.at(i)) {
        //     indices.push_back(DV_index.at(i));
        //   }
        // }
        // return indices;
        // equivalent to:
        return DV_index[DV_passChiSqCut == 1 and DV_passDistCut == 1 and
                        DV_passMaterialVeto_strict == 1];
      };

  auto good_dv_sel_df = rdf.Define("good_dv_idx", good_dv_index_selector,
                                   {"DV_passChiSqCut", "DV_passDistCut",
                                    "DV_passMaterialVeto_strict", "DV_index"})
                            .Define("n_good_dvs",
                                    [](const RVecUL& DV_index) {
                                      return static_cast<int>(DV_index.size());
                                    },
                                    {"good_dv_idx"});

  auto good_dv_sel_df2 =
      rdf.Define("good_dv_idx",
                 "DV_index[DV_passChiSqCut==1 && DV_passDistCut==1 && "
                 "DV_passMaterialVeto_strict==1]")
          .Define("n_good_dvs", "good_dv_idx.size()"); // does the same, slower

  auto h_ndvs = good_dv_sel_df.Histo1D(
      {"n_good_dvs", ";Number of good DVs per event;Entries", 10, 0, 10},
      "n_good_dvs");

  auto h_ndvs2 = good_dv_sel_df2.Histo1D(
      {"n_good_dvs2", ";Number of good DVs per event;Entries", 10, 0, 10},
      "n_good_dvs");

  auto h_presel = presel_df.Histo1D(
      {"MET_phi_presel", ";#phi_{MET};Entries", 100, -3.6, 3.6}, "MET_phi");
  auto h = rdf.Histo1D(
      {"MET_phi", ";#phi_{{E}_{ T}^{ miss}};Entries", 100, -3.6, 3.6},
      "MET_phi");

  // DV pre - selection : DV_passFiducialCut == 1 && DV_passChiSqCut == 1 &&
  //     DV_passDistCut == 1 &&
  //     DV_passMaterialVeto_strict ==
  //         1 DV full selection(N - trk > 5 and m > 10 GeV)
  //     : DV_passNTrkCut == 1 && DV_passMassCut ==
  //       1

  // select good dvs
  auto dv_pre_selector =
      [](const RVecB& DV_passFiducialCut, const RVecB& DV_passChiSqCut,
         const RVecB& DV_passDistCut, const RVecB& DV_passMaterialVeto_strict,
         const RVecUL& DV_index) {
        return DV_index[DV_passChiSqCut == 1 and DV_passDistCut == 1 and
                        DV_passMaterialVeto_strict == 1]
                   .size() > 1;
      };

  auto dv_full_selector = [](bool dv_pre_sel, const RVecB& DV_passNTrkCut,
                             const RVecB& DV_passMassCut,
                             const RVecUL& DV_index) {
    return dv_pre_sel and
           DV_index[DV_passNTrkCut == 1 and DV_passMassCut == 1].size() > 1;
  };

  auto newvars_rdf =
      rdf.Define("dv_pre_sel", dv_pre_selector,
                 {"DV_passFiducialCut", "DV_passChiSqCut", "DV_passDistCut",
                  "DV_passMaterialVeto_strict", "DV_index"})
          .Define(
              "dv_full_sel", dv_full_selector,
              {"dv_pre_sel", "DV_passNTrkCut", "DV_passMassCut", "DV_index"});

  // We book the action: it will be treated during the event loop.
  auto eff_sel_ndv = good_dv_sel_df.Book<unsigned char, int>(
      TEffBooker{"sel_eff_ngood_dvs",
                 ";Number of good DVs per event;Selection efficiency", 10, 0,
                 10},
      {"BaselineSel_HighPtSR", "n_good_dvs"});

  auto eff_sel_aipbc = good_dv_sel_df.Book<unsigned char, float>(
      TEffBooker{"sel_eff_interactions",
                 ";Actual interactions per BC;Selection efficiency", 80, 0, 80},
      {"BaselineSel_HighPtSR", "actualInteractionsPerCrossing"});

  TFile* output_file = TFile::Open("outputfile.root", "RECREATE");
  output_file->cd();

  TStopwatch timer;
  timer.Start();
  h_presel->Write();
  timer.Stop();
  timer.Print();
  timer.Start();
  h->Write();
  timer.Stop();
  timer.Print();
  timer.Start();
  h_ndvs->Write();
  timer.Stop();
  timer.Print();
  timer.Start();
  h_ndvs2->Write();
  timer.Stop();
  timer.Print();

  eff_sel_ndv->Write();
  eff_sel_aipbc->Write();

  output_file->Close();

  std::cout << "done!\n";

  std::cout << "\nBye bye\n";
}
