#include "CommonHelpers.h"
#include "Configurator.h"

void checkFileslistIsGood() {
  std::cout << "Welcome to checkFileslistIsGood\n";

  std::cout << " - Reading the topConfig\n";
  AL::Configurator config(TString("../topConfig.cfg"));

  std::cout << " - Reading the input files list\n";
  std::vector<TString> files_list =
      AL::getListOfFiles(config.getStr("data_paths_file"));

  std::cout << "INFO: Found " << files_list.size() << " input files.\n";

  std::cout << "Done, bye!\n";
}
