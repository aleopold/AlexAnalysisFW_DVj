#include "CommonHelpers.h"
#include "Configurator.h"

void testVectorize() {

  std::string s =
      "                             "
      "/eos/atlas/atlascerngroupdisk/phys-susy/DVjets_ANA-SUSY-2018-13/"
      "ntuples_210616/"
      "group.phys-susy.user.cohm.mc16_13TeV.245bc66d.210616_trees.root/"
      "group.phys-susy.448134.e7245_s3126_r10739_r10706_p4302.25903063._000785."
      "trees.root "
      "/eos/atlas/atlascerngroupdisk/phys-susy/DVjets_ANA-SUSY-2018-13/"
      "ntuples_210616/"
      "group.phys-susy.user.cohm.mc16_13TeV.245bc66d.210616_trees.root/"
      "group.phys-susy.448134.e7245_s3126_r10740_r10706_p4302.25903063._001530."
      "trees.root "
      "/eos/atlas/atlascerngroupdisk/phys-susy/DVjets_ANA-SUSY-2018-13/"
      "ntuples_210616/"
      "group.phys-susy.user.cohm.mc16_13TeV.245bc66d.210616_trees.root/"
      "group.phys-susy.448134.e7245_s3126_r10790_r10726_p4302.25903063._001645."
      "trees.root";

  std::vector<TString> v = AL::vectorize(TString(s), " ");
  std::cout << v.size() << '\n';

  AL::Configurator cfg(
      TString("/data/aleopold/dvjets/output_histograms/"
              "output_20220120_212126/mc/metadata.mc.448114.cfg"));

  std::vector<TString> v2 = AL::vectorize(cfg.getStr("input_files", "default"), " ");
  std::cout << "::" << v2.at(v2.size()-1) << "::\n";
  std::cout << "::" << cfg.getStr("input_files") << "::\n";
  std::cout << v2.size() << '\n';
}
