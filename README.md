


run within `scripts` folder, this takes ~ 10 minutes
```
python findFilesWithSameRN.py
```

Depending if you want to update/create this for data or MC only, comment out the respective methods
TODO: -> move this into individual files or add a command line switch


create the folder structure and config files

```
python createHistos.py
```


for data, run this on condor

```
condor_submit condorsubmission.sub
```
this will send ~ 800 - 900 jobs


once the data is processed, merge the output files into the data 15/16/17/18 parts
Depending if you want to update/create this for data or MC only, comment out the respective methods
TODO: -> move this into individual files or add a command line switch
```
python mergeFiles.py
```
