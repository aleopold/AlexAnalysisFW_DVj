import inputFinder_dvj as finder
import simpleHelperFncs as helper

import ROOT
import os
import subprocess
import multiprocessing


def callCmd(cmd, id, sema):
    import subprocess
    try:
        with subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True) as p:
            out, err = p.communicate()
        print('release the Semaphore of', id)
        sema.release()
    except:
        print("ERROR: crash in ", id)
        sema.release()

def mergeMCPerDSID(config, grid):
    from multiprocessing import Process
    from multiprocessing import Semaphore
    dsid_file_dict = finder.createGridMCDictionary(grid)

    target_foler = 'mc'
    periods = ['mc16a', 'mc16d', 'mc16e']

    path = config.GetValue("current_output_path", ".")

    concurrency = 10
    sema = Semaphore(concurrency)
    all_processes = []

    for dsid_key in dsid_file_dict:
        targetpath = os.path.join(path, 'mc')
        targetfile = 'output.mc.{}.root'.format(dsid_key)
        fulltarget = os.path.join(targetpath, targetfile)
        hadd_cmd = 'hadd -f {target} '.format(target=fulltarget)
        for period in periods:
            foldername = os.path.join(path, period)
            filename = 'output.{}.{}.root'.format(period, dsid_key)
            fullname = os.path.join(foldername, filename)
            if not os.path.exists(fullname):
                print("WARNING: file does not exist: ", fullname)
                continue
            hadd_cmd += fullname
            hadd_cmd += ' '
        print(hadd_cmd)
        sema.acquire()
        p = Process(target=callCmd, args=(hadd_cmd, dsid_key, sema))
        all_processes.append(p)
        p.start()
        # subprocess.call(hadd_cmd, shell=True)

    for p in all_processes:
        p.join()

def mergeDataPerPeriod(config):
    from multiprocessing import Process
    from multiprocessing import Semaphore

    json_file = os.path.join(os.getenv('ANA_FW_PATH'), config.GetValue("data_files_per_rn_file", ""))
    json_file = json_file.format(config.GetValue("data_version", ""))
    # json_file = os.path.join(os.getenv('ANA_FW_PATH'), config.GetValue("data_files_per_rn_file", ""))
    files_dict = finder.dataFilesPerPeriod(json_file)

    concurrency = 10
    sema = Semaphore(concurrency)
    all_processes = []

    path = config.GetValue("current_output_path", ".")

    for data_period in files_dict:
        if data_period == "data1516":
            continue
        # print("data_period", data_period)
        targetpath = os.path.join(path, 'data')
        targetfile = 'output.{}.root'.format(data_period)
        fulltarget = os.path.join(targetpath, targetfile)

        hadd_cmd = 'hadd -f {target} '.format(target=fulltarget)

        foldername = os.path.join(path, data_period)

        for runnumber in files_dict[data_period]:
            # print("runnumber", runnumber)
            filename = 'output.{}.{}.root'.format(data_period, runnumber)
            fullname = os.path.join(foldername, filename)
            if not os.path.exists(fullname):
                print("WARNING: file does not exist: ", fullname)
                continue
            hadd_cmd += fullname
            hadd_cmd += ' '
        print("hadding: ", targetfile)
        sema.acquire()
        p = Process(target=callCmd, args=(hadd_cmd, data_period, sema))
        all_processes.append(p)
        p.start()
        subprocess.call(hadd_cmd, shell=True)

    for p in all_processes:
        p.join()

def main():
    print("Starting mergeFiles")

    print("Read top config")

    top_config_filename = "topConfig.cfg"

    config = ROOT.TEnv(top_config_filename)

    mergeMCPerDSID(config, 'GG_qqn1-rpvLF')

    mergeDataPerPeriod(config)

if __name__ == "__main__":
    main()
