#include "CommonHelpers.h"
#include "Configurator.h"
#include "HelperFunctions.cxx"

#include <cstdint>

#include <nlohmann/json.hpp>

TH1F* createHistogram(TString path, TString period) {

  std::cout << "createHistogram for period " << period << '\n';

  unsigned int minRN, maxRN;
  std::map<unsigned int, double> lumidata;
  double rel_lumi_uncertainty = 2.0; // 1.9 for 2015

  if (period == "201516") {
    auto map15 = readLuminosityData(path, "2015.csv");
    auto map16 = readLuminosityData(path, "2016.csv");
    // lumidata = map15.insert(map16.begin(), map16.end());
    lumidata.insert(map15.begin(), map15.end());
    lumidata.insert(map16.begin(), map16.end());
    minRN = 266904;
    maxRN = 311481;
  } else if (period == "2015") {
    auto map = readLuminosityData(path, "2015.csv");
    lumidata.insert(map.begin(), map.end());
    minRN = 266904;
    maxRN = 284484;
  }else if (period == "2016") {
    auto map = readLuminosityData(path, "2016.csv");
    lumidata.insert(map.begin(), map.end());
    minRN = 297730;
    maxRN = 311481;
  }else if (period == "2017") {
    auto map = readLuminosityData(path, "2017.csv");
    lumidata.insert(map.begin(), map.end());
    minRN = 324320;
    maxRN = 341649;
  } else if (period == "2018") {
    auto map = readLuminosityData(path, "2018.csv");
    lumidata.insert(map.begin(), map.end());
    minRN = 348197;
    maxRN = 364485;
  }

  std::cout << "lumi file read\n";

  AL::Configurator config(
      TString("/home/aleopold/jet_plus_dv/analysis_fw/topConfig.cfg"));

  std::vector<TString> files_list =
      AL::getListOfFilesNoCheck(config.getStr("data_paths_file"));

  cleanBadFiles(files_list);

  TChain* chain = AL::getChain(files_list, "trees_SRDV_", -1);

  ROOT::RDataFrame rdf(*chain);

  auto runNumberCmpr = [&minRN, &maxRN](unsigned int rn) {
    return rn >= minRN and rn <= maxRN;
  };

  // runnumber, entries
  std::map<unsigned int, unsigned int> events_in_run;

  auto accumulateEventsInRN = [&events_in_run](unsigned int rn) {
    events_in_run[rn] += 1;
    // std::cout << "RunNumber " << rn << '\n';
  };

  auto presel_peroiod_df = rdf.Filter("BaselineSel_HighPtSR==1")
                               .Filter(runNumberCmpr, {"runNumber"});
                               // .Range(0, 1000);

  std::cout << "filters set, running foreach...\n";

  presel_peroiod_df.Foreach(accumulateEventsInRN, {"runNumber"});

  auto runnumber_yields =
      new TH1F("runnumber_yields", ";; Lumi-normalized yields (Events / pb)",
               events_in_run.size(), 0, events_in_run.size());

  std::cout << "set bin names \n";

  int i = 0;
  for (auto const& [key, value] : events_in_run) {
    std::cout << " key=" << key;
    std::cout << " value=" << value << std::endl;
    TString bin_name = Form("%i", value);
    runnumber_yields->GetXaxis()->SetBinLabel(i + 1, bin_name);
    i++;
  }

  std::cout << "set bin entries \n";

  // for (size_t i = 0; i < events_in_run.size(); i++) {
  i = 0;
  for (auto const& [key, value] : events_in_run) {
    int runnum = key;
    double events = value;
    double lumi = lumidata[runnum];
    double content = events / lumi;
    runnumber_yields->SetBinContent(i + 1, content);
    // assume 3.2% relative error
    // sigma**2 = 1/L**2 * (N + y**2 sigmaL**2), y=N/L
    double dfdl = events / (lumi * lumi);
    double dfdn = 1. / lumi;
    double event_error2 = events; // sqrt(events);
    double lumi_error = lumi * rel_lumi_uncertainty / 100.;
    double error = sqrt(dfdn * dfdn * event_error2 +
                        dfdl * dfdl * lumi_error * lumi_error);
    runnumber_yields->SetBinError(i + 1, error);
    i++;
  }

  std::cout << "return \n";

  return runnumber_yields;
}

void produceLumiNormalizedYields() {

  //////////////////////////////////////////////////
  //////////////////////////////////////////////////

  TString path = "/home/aleopold/jet_plus_dv/analysis_fw/data/";

  auto h201516 = createHistogram(path, "201516");
  // auto h2017 = createHistogram(path, "2017");
  // auto h2018 = createHistogram(path, "2018");

  TFile* ofile = TFile::Open("lumiNormData.root", "RECREATE");

  h201516->Write();
  // h2017->Write();
  // h2018->Write();

  ofile->Close();

  // lumi unc:
  // 15:1.9, 16:2.2, 17:2.4, 18:2.0
}
