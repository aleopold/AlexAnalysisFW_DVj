// clang-format off
/*
*
* root -l -q -b 'macroLumiNormYield("/eos/user/a/aleopold/jetdv/outputs/output_20220126_155731/plot_tmp/lumiNormYield.root")'
*/
// clang-format on

#include "PlottingUtils.h"
#include "PlottingStyle.h"
#include "Configurator.h"
#include "CommonHelpers.h"

void testNewLumiMaps() {
  auto file = TFile::Open(
      "/home/aleopold/jet_plus_dv/factory_tools_fw/src/FactoryTools/"
      "FactoryTools/data/"
      "ilumicalc_histograms_None_276262-311401_OflLumi-13TeV-005.root",
      "READ");

  TTreeReader reader("LumiMetaData", file);

  TTreeReaderValue<unsigned int> run_number(reader, "RunNbr");
  TTreeReaderValue<float> int_lumi(reader, "IntLumi");
  TTreeReaderValue<float> total_lumi(reader, "TotalLumi");
  TTreeReaderValue<unsigned int> lb_start(reader, "LBStart");

  while (reader.Next()) {
    std::cout << "RN: " << *run_number << " IntLumi: " << *int_lumi
              << " TotalLumi: " << *total_lumi << " LBStart: " << *lb_start
              << '\n';
  }
}

void macroLumiNormYield(TString cfg_name = "", TString filename = "") {

  // testNewLumiMaps();
  // return;

  AL::Configurator config(cfg_name);

  auto file = TFile::Open(filename, "READ");

  AL::StyleSetter stye_setter;
  stye_setter.left_margin = 0.1;
  stye_setter.title_y_offset = 0.8;
  stye_setter.title_y_offset = 1.;
  stye_setter.title_x_offset = 2.2;
  stye_setter.label_x_offset = 0.02;
  stye_setter.bottom_margin = 0.25;
  // stye_setter.font_size = 0.035;

  AL::setVariableStyle(stye_setter);

  std::vector<TString> periods = {"2015", "2016", "2017", "2018"};
  // std::vector<TString> selections = {"HighPTPre", "HighPTVR", "HighPTSR",
  // "TrklessPre", "any"};
  // std::vector<TString> selections = {
  //     "HighPTPre",  "HighPTVR",     "HighPTSR", "TrklessPre",
  //     "noprescale", "HighPT_OCCUP", "any",      "HighPTPreNEW"};
  std::vector<TString> selections = {"HighPTPreNEW"};

  for (auto selection : selections) {
    for (auto period : periods) {
      TString histoname = Form("histo%s%s", period.Data(), selection.Data());
      auto hist = file->Get<TH1F>(histoname);
      if (not hist) {
        std::cout << histoname << " does not exist, continue\n";
        continue;
      }
      hist->SetAxisRange(-5, 150, "Y");

      hist->GetXaxis()->SetTitle("Run number");
      hist->GetYaxis()->SetTitle("Events / Luminosity [pb]");
      hist->GetXaxis()->SetLabelSize(0.035);

      auto canvas = new TCanvas("", "", 1800, 500);

      if (selection == "TrklessPre") {
        hist->SetAxisRange(-5, 150, "Y");
      } else if (selection == "HighPTSR") {
        hist->SetAxisRange(-5, 10, "Y");
      } else if (selection == "any") {
        hist->SetAxisRange(-5, 500, "Y");
      } else if (selection == "HighPT_OCCUP") {
        hist->SetAxisRange(-1, 15, "Y");
      }

      hist->Draw();

      Color_t textcolor = kBlack;

      AL::ATLAS_LABEL(0.14, 0.83, textcolor, 2.0);
      AL::myText(0.225, 0.83, textcolor, "Internal", 0.1);

      TString data_info = Form("Data period: %s", period.Data());
      AL::myText(0.14, 0.78, textcolor, data_info.Data());
      if (selection.Contains("SR")) {
        TString selection_str =
            Form("Selection level: %s (blinded)", selection.Data());
        AL::myText(0.14, 0.73, textcolor, selection_str.Data());
      } else if (selection == "any") {
        TString selection_str = "Selection: Pass DRAW jet requirement, pass "
                                "HighPt or Trackless jet selection";
        AL::myText(0.14, 0.73, textcolor, selection_str.Data());
      } else {
        TString selection_str = Form("Selection level: %s", selection.Data());
        AL::myText(0.14, 0.73, textcolor, selection_str.Data());
      }

      TString plot_path = config.getStr("output_plot_path");

      AL::preparePlottingDir(config, "output_plot_path");

      TString plot_name = Form("lumiNormedEvnts.%s.pdf", histoname.Data());
      TString plot_full_path =
          Form("%s/%s", plot_path.Data(), plot_name.Data());
      canvas->Print(plot_full_path);

      plot_full_path.ReplaceAll(".pdf", ".png");
      canvas->Print(plot_full_path);
    }
  }
}
