// clang-format off
/*
*
* root -l -q -b 'macroSelectionEfficiency.cxx("/eos/user/a/aleopold/jetdv/outputs/output_20220202_094701/mc16e/metadata.mc16e.448149.cfg")'
*/
// clang-format on

#include "PlottingUtils.h"
#include "PlottingStyle.h"
#include "Configurator.h"
#include "CommonHelpers.h"

void plot(TFile* file, AL::Configurator& config, std::vector<TString> regions,
          std::vector<Color_t> colors, TString name_ext, bool do_fit = false,
          bool log_scale = false) {

  gROOT->SetStyle("ATLAS");
  // AL::StyleSetter stye_setter;
  // AL::setVariableStyle(stye_setter);

  auto canvas = new TCanvas();
  // canvas->SetLogy();
  auto axishist = new TH1F(
      "axishist", ";Avg. interactions per BC;Selection Efficiency", 10, 5, 65);

  if (log_scale) {
    axishist->SetAxisRange(0.000001, 100, "Y");
  } else {
    axishist->SetAxisRange(0, 1.5, "Y");
  }
  axishist->Draw();

  std::vector<TObject*> eff_objects;
  std::vector<TF1*> fits;

  float max_eff = 0;
  float min_eff = 1;

  for (size_t i = 0; i < regions.size(); i++) {
    TString effname = Form("%s/eff_sel_vs_avrg_int", regions.at(i).Data());
    TEfficiency* eff = file->Get<TEfficiency>(effname);
    eff_objects.push_back(eff);
    eff->SetLineColor(colors.at(i));
    eff->SetMarkerColor(colors.at(i));
    eff->SetMarkerStyle(22);
    eff->SetMarkerSize(1);
    eff->SetLineWidth(1);
    eff->Draw("SAME");

    // linear fit
    if (do_fit) {
      TF1* f1 = new TF1(effname + "_fit", "[0]+[1]*x", 15, 55);
      f1->SetParameters(0.5, -0.01);
      f1->SetLineColor(colors.at(i));
      f1->SetMarkerSize(0);
      f1->SetMarkerColor(colors.at(i));
      // eff->Fit(f1);
      // TH1F* hist = const_cast<TH1F*>(dynamic_cast<const
      // TH1F*>(eff->GetTotalHistogram()));
      TGraphAsymmErrors* graph = eff->CreateGraph();
      graph->Fit(f1, "", "", 15, 55);
      std::cout << "FIT: " << f1->GetParameter(0) << " " << f1->GetParameter(1)
                << '\n';
      fits.push_back(f1);
      f1->Draw("SAME");
    }

    int n_bins = eff->GetTotalHistogram()->GetNbinsX();
    for (int bin = 1; bin < n_bins; bin++) {
      float bin_eff = eff->GetEfficiency(bin);
      if (bin_eff > max_eff) {
        max_eff = bin_eff;
      }
      if (bin_eff < min_eff) {
        min_eff = bin_eff;
      }
    }
  }

  if (log_scale) {
    axishist->SetAxisRange(0.000001, 100, "Y");
  } else {
    if (max_eff == 0.) {
      axishist->SetAxisRange(0, 1.0, "Y");
    } else {
      axishist->SetAxisRange(0, 1.7 * max_eff, "Y");
    }
  }

  TString dsid = config.getStr("DSID", "default");
  TString mc_period = config.getStr("mc_period", "default");
  if (mc_period == "mc") {
    mc_period = "all";
  }
  TString pretty_sample_name = config.getStr("legend", "default");
  TString grid_name = config.getStr("grid", "default");
  TString pretty_sample_name_short = config.getStr("legendShort", "default");

  Color_t textcolor = kBlack;

  AL::ATLAS_LABEL(0.2, 0.86, textcolor, 1.25);
  AL::myText(0.35, 0.86, textcolor, "Internal", 0.06);

  // Sample and period

  // TString dsid_info = Form("DSID: %s", dsid.Data());
  // AL::myText(0.2, 0.8, textcolor, dsid_info.Data(), 0.05);
  // TString mc_period_info = Form("MC period: %s", mc_period.Data());
  // AL::myText(0.2, 0.74, textcolor, mc_period_info.Data(), 0.05);
  // AL::myText(0.2, 0.68, textcolor, "Trackless", 0.05);

  // Sample and period
  AL::myText(0.2, 0.78, textcolor, pretty_sample_name.Data(), 0.035);

  TString info = Form("DSID: %s, MC period: %s", dsid.Data(), mc_period.Data());
  AL::myText(0.64, 0.89, textcolor, info.Data(), 0.03);

  // info = Form("Grid: %s,  %s", grid_name.Data(),
  // pretty_sample_name_short.Data()); AL::myText(0.58, 0.85, textcolor,
  // info.Data(), 0.03);

  auto legend = new TLegend(0.52, 0.87, 0.9, 0.7);
  legend->SetTextFont(42);
  legend->SetFillStyle(0);
  legend->SetBorderSize(0);
  size_t n_regions = regions.size();
  if (n_regions <= 3) {
    legend->SetTextSize(0.035);
  } else if (n_regions == 4) {
    legend->SetTextSize(0.03);
  } else {
    legend->SetTextSize(0.035);
  }

  for (size_t i = 0; i < regions.size(); i++) {
    if (regions.at(i) == "HighPT_OCCUP") {
      legend->AddEntry(eff_objects.at(i),
                       "HighPT, n_{tracks} #leq 4, m_{DV} < 10 GeV", "lp");
    } else if (regions.at(i) == "HighPTPre") {
      legend->AddEntry(eff_objects.at(i), "HighPT, jet level pre-selection",
                       "lp");
    } else if (regions.at(i) == "HighPTSR") {
      legend->AddEntry(eff_objects.at(i), "HighPT, DV signal region", "lp");
    } else if (regions.at(i) == "TrklessPre") {
      legend->AddEntry(eff_objects.at(i), "Trackless, jet level pre-selection",
                       "lp");
    } else if (regions.at(i) == "TrklessPre") {
      legend->AddEntry(eff_objects.at(i), "Trackless, jet level pre-selection",
                       "lp");
    } else {
      legend->AddEntry(eff_objects.at(i), regions.at(i), "lp");
    }
  }

  legend->Draw("same");

  if (do_fit) {

    // AL::myText(0.2, 0.7, textcolor, "Fits:", 0.03);
    auto legend_fits = new TLegend(0.2, 0.72, 0.45, 0.65);
    if (n_regions > 3) {
      legend_fits->SetNColumns(2);
    }
    // auto legend_fits = new TLegend();
    legend_fits->SetTextFont(42);
    legend_fits->SetHeader("Fits for #mu #in [15, 55]:", "C");
    // legend_fits->SetTextFont(42);
    legend_fits->SetFillStyle(0);
    // legend_fits->SetFillColor(kBlue);
    legend_fits->SetBorderSize(0);
    // legend_fits->SetTextAlign(13);

    if (n_regions <= 3) {
      legend_fits->SetTextSize(0.02);
    } else if (n_regions == 4) {
      legend_fits->SetTextSize(0.02);
    } else {
      legend_fits->SetTextSize(0.02);
    }

    for (size_t i = 0; i < regions.size(); i++) {
      float p0 = fits.at(i)->GetParameter(0);
      float p1 = fits.at(i)->GetParameter(1);
      float loss = ((p0 + 15. * p1) - (p0 + 50 * p1)) / (p0 + 15. * p1);
      std::cout << "eff at 15: " << (p0 + 15. * p1)
                << " eff at 50: " << (p0 + 50 * p1) << '\n';
      TString leg_str =
          Form("%.5f + x * %.5f, eff. loss from #mu=15 to #mu=50: %.1f%%", p0,
               p1, (loss)*100.);
      legend_fits->AddEntry(fits.at(i), leg_str, "lp");
    }

    legend_fits->Draw("same");
  }

  gPad->RedrawAxis();

  // TString ana_folder = std::getenv("ANA_FW_PATH");
  // TString output_plot_path = Form("%s/PLOTS", ana_folder.Data());

  AL::preparePlottingDir(config, "output_plot_path");

  TString output_plot_path = config.getStr("output_plot_path", "default");

  std::cout << "Plot will be written to: " << output_plot_path << '\n';

  // gSystem->Exec(Form("mkdir -p %s", output_plot_path.Data()));

  TString plot_name = Form("selectionEfficiency.%s.%s.%s.pdf", name_ext.Data(),
                           dsid.Data(), mc_period.Data());
  TString plot_full_path =
      Form("%s/%s", output_plot_path.Data(), plot_name.Data());
  canvas->Print(plot_full_path);

  plot_full_path.ReplaceAll(".pdf", ".png");
  canvas->Print(plot_full_path);
}

void macroSelectionEfficiency(TString cfg_name = "") {

  AL::Configurator config(cfg_name);

  std::cout << "Welcome to macroSelectionEfficiency\n";
  TString dsid = config.getStr("DSID", "default");
  TString pretty_sample_name = config.getStr("legend", "default");
  TString mc_period = config.getStr("mc_period", "default");
  std::cout << "working on DSID: " << dsid << " MC period: " << mc_period
            << '\n';
  TString current_output_path = config.getStr("current_output_path", "default");
  TString rootfilename = config.getStr("output_filename", "default");

  TString filename = Form("%s/%s/%s", current_output_path.Data(),
                          mc_period.Data(), rootfilename.Data());

  auto file = TFile::Open(filename, "READ");

  // plot(file, config, {"HighPTPre", "TrklessPre", "HighPT_OCCUP", "HighPTSR"},
  // {kRed, kBlue, kGreen, kCyan}, "all");

  plot(file, config, {"HighPTPre", "HighPTSR", "HighPT_OCCUP"},
       {kRed, kBlue, kGreen}, "HighPT");

  plot(file, config, {"HighPTSR"}, {kBlue}, "HighPTSR", true);

  file->Close();
}
