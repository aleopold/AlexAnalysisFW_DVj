// clang-format off
/*
*
*/
// clang-format on

#include "PlottingUtils.h"
#include "PlottingStyle.h"
#include "Configurator.h"
#include "CommonHelpers.h"


void macroTrigFracPerRN(TString cfg_name = "", TString filename = "") {
  AL::Configurator config(cfg_name);

  auto file = TFile::Open(filename, "READ");

  AL::StyleSetter stye_setter;
  stye_setter.left_margin = 0.1;
  stye_setter.title_y_offset = 0.8;
  stye_setter.title_y_offset = 1.;
  stye_setter.title_x_offset = 2.2;
  stye_setter.label_x_offset = 0.02;
  stye_setter.bottom_margin = 0.25;

  AL::setVariableStyle(stye_setter);

  std::vector<TString> periods = {"2015", "2016", "2017", "2018"};
  std::vector<TString> selections = {"HighPTPre"};
  std::vector<std::string> trigger_names = {
      "pass_EF_6j70",
      "pass_HLT_2j220_j120",
      "pass_HLT_2j250_j120",
      "pass_HLT_2j275_j140",
      "pass_HLT_4j100",
      "pass_HLT_4j110",
      "pass_HLT_4j120",
      "pass_HLT_4j130",
      "pass_HLT_5j100",
      "pass_HLT_5j60",
      "pass_HLT_5j65_0eta240_L14J150ETA25",
      "pass_HLT_5j70",
      "pass_HLT_5j75_0eta250",
      "pass_HLT_5j85",
      "pass_HLT_5j85_lcw",
      "pass_HLT_5j90",
      "pass_HLT_6j45",
      "pass_HLT_6j45_0eta240",
      "pass_HLT_6j55_0eta240_L14J150ETA25",
      "pass_HLT_6j60",
      "pass_HLT_6j70",
      "pass_HLT_6j85",
      "pass_HLT_7j45",
      "pass_HLT_7j50",
      "pass_HLT_g140_loose"};

  for (auto selection : selections) {
    for (auto period : periods) {
      for (auto trigger_name : trigger_names) {
        TString histoname = Form("histo%s%s%s", period.Data(), selection.Data(),
                                 trigger_name.c_str());
        auto hist = file->Get<TH1F>(histoname);
        hist->SetAxisRange(0., 2.0, "Y");

        hist->GetXaxis()->SetTitle("Run number");
        hist->GetYaxis()->SetTitle("Trigger rate");
        hist->GetXaxis()->SetLabelSize(0.035);

        auto canvas = new TCanvas("", "", 1800, 500);

        hist->Draw();

        Color_t textcolor = kBlack;

        AL::ATLAS_LABEL(0.14, 0.83, textcolor, 2.0);
        AL::myText(0.225, 0.83, textcolor, "Internal", 0.1);

        TString data_info = Form("Data period: %s", period.Data());
        AL::myText(0.14, 0.78, textcolor, data_info.Data());
        if (selection.Contains("SR")) {
          TString selection_str =
              Form("Selection level: %s (blinded)", selection.Data());
          AL::myText(0.14, 0.73, textcolor, selection_str.Data());
        } else {
          TString selection_str = Form("Selection level: %s", selection.Data());
          AL::myText(0.14, 0.73, textcolor, selection_str.Data());
        }

        TString trg_name = Form("Trigger: %s", trigger_name.c_str());
        AL::myText(0.55, 0.83, textcolor, trg_name.Data());


        TString plot_path = config.getStr("output_plot_path");

        AL::preparePlottingDir(config, "output_plot_path");

        TString plot_name = Form("trigger_rates.%s.pdf", histoname.Data());
        TString plot_full_path =
            Form("%s/%s", plot_path.Data(), plot_name.Data());
        canvas->Print(plot_full_path);

        plot_full_path.ReplaceAll(".pdf", ".png");
        canvas->Print(plot_full_path);
      }
    }
  }
}
