import inputFinder_dvj as finder

import ROOT

from array import array
import json
import os

def doitForData(config):
    """
    Write a json file in the form of
    {
        ....,
        "279169": [
        "/eos/atlas/atlascerngroupdisk/phys-susy/DVjets_ANA-SUSY-2018-13/ntuples_210616/group.phys-susy.user.cohm.data1516.245bc66d.210909_trees.root/group.phys-susy.00279169.r11969_r11784_p4072_p4296.26635556._000070.trees.root",
        "/eos/atlas/atlascerngroupdisk/phys-susy/DVjets_ANA-SUSY-2018-13/ntuples_210616/group.phys-susy.user.cohm.data1516.245bc66d.210909_trees.root/group.phys-susy.00279169.r11969_r11784_p4072_p4296.26635556._000074.trees.root"
      ],
      "279259": [
        "/eos/atlas/atlascerngroupdisk/phys-susy/DVjets_ANA-SUSY-2018-13/ntuples_210616/group.phys-susy.user.cohm.data1516.245bc66d.210909_trees.root/group.phys-susy.00279259.r11969_r11784_p4072_p4296.26635556._000370.trees.root"
      ],
      ....
    }
    to the data folder
    """
    data_input_files = finder.getInputDataNtuplesFileList(config)
    # data_input_files = [data_input_files[i] for i in range(1,10)]
    ##open each file
    rn_file_dict = {}

    data_version = config.GetValue("data_version", ".")

    for file in data_input_files:
        ##get the ttree
        root_file = ROOT.TFile.Open(file, "READ")
        tree = root_file.Get(finder.treeName())

        ## read the first run number entry
        run_number = array('i', [0])
        try:
            tree.SetBranchAddress('runNumber', run_number)
        except:
            print("file: ", file, "does not contain a run number")
            root_file.Close()
            continue

        tree.GetEntry(1)

        rn_start = run_number[0]

        if not (run_number[0] in rn_file_dict.keys()):
            rn_file_dict[run_number[0]] = [file]
        else:
            rn_file_dict[run_number[0]].append(file)

        tree.GetEntry(tree.GetEntries()-1)
        if (run_number[0] != rn_start):
            print("OH SHIT, RunNumber is changing in file", file)

        ## close rootfile
        root_file.Close()
    for rn in rn_file_dict:
        print(rn, len(rn_file_dict[rn]))

    ##write out config file for each run number
    data_json_name = config.GetValue("data_files_per_rn_file", ".").format(data_version)
    json_file_name = os.path.join(os.getenv('ANA_FW_PATH'), data_json_name)
    # with open("/home/aleopold/jet_plus_dv/analysis_fw/data/dataNtupleFilesPerRunNumber.json", 'w')
    with open(json_file_name, 'w') as fout:
        json_dumps_str = json.dumps(rn_file_dict, indent=2)
        print(json_dumps_str, file=fout)

def doitForMC(config, grid):
    dsid_file_dict = finder.createGridMCDictionary(grid)

    file_per_rn_dict = {};
    for dsid in dsid_file_dict:
        files = dsid_file_dict[dsid]

        dsid_dict = {}
        for file in files:
            ##get the ttree
            root_file = ROOT.TFile.Open(file, "READ")
            tree = root_file.Get(finder.treeName())

            ## read the first run number entry
            run_number = array('i', [0])
            try:
                tree.SetBranchAddress('runNumber', run_number)
            except:
                print("file: ", file, "does not contain a run number")
                root_file.Close()
                continue

            tree.GetEntry(1)

            rn_start = run_number[0]

            if not (run_number[0] in dsid_dict.keys()):
                dsid_dict[run_number[0]] = [file]
            else:
                dsid_dict[run_number[0]].append(file)

            tree.GetEntry(tree.GetEntries()-1)
            if (run_number[0] != rn_start):
                print("OH SHIT, RunNumber is changing in file", file)
                print("rn_start", rn_start, "run_number", run_number[0])

            ## close rootfile
            root_file.Close()
        print("done with", dsid)
        file_per_rn_dict[dsid] = dsid_dict

    ##write out config file for each run number
    json_file_name = os.path.join(os.getenv('ANA_FW_PATH'), "data/mcNtupleFilesPerDSIDPerRunNumber.{grid}.json".format(grid=grid))

    print("Creating: ", json_file_name)

    # with open("/home/aleopold/jet_plus_dv/analysis_fw/data/dataNtupleFilesPerRunNumber.json", 'w')
    with open(json_file_name, 'w') as fout:
        json_dumps_str = json.dumps(file_per_rn_dict, indent=2)
        print(json_dumps_str, file=fout)





def main():
    """
    This takes quite a bit to run...
    """

    top_config_filename = os.path.join(os.getenv('ANA_FW_PATH'), "topConfig.cfg")
    config = ROOT.TEnv(top_config_filename)

    # doitForData(config)
    doitForMC(config, 'GG_qqn1-rpvLF')



if __name__ == "__main__":
    main()
