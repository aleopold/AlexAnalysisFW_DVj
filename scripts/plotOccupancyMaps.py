import simpleHelperFncs as hlpr

import json
import os
import math
import subprocess

import ROOT

ROOT.gROOT.SetStyle('ATLAS')


def main():
    """
    Uses period-combined data files from "data" folder
    """
    
    top_config_filename = os.path.join(os.getenv('ANA_FW_PATH'), "topConfig.cfg")

    config = ROOT.TEnv(top_config_filename)

if __name__ == "__main__":
    main()
