import inputFinder_dvj as finder
import simpleHelperFncs as helper

import os

import ROOT

import multiprocessing
import subprocess

def main():
    top_config_filename = os.path.join(os.getenv('ANA_FW_PATH'), "topConfig.cfg")

    config = ROOT.TEnv(top_config_filename)

    path = config.GetValue("current_output_path", "")

    concurrency = 10
    sema = multiprocessing.Semaphore(concurrency)
    all_processes = []

    ### I need the config file of each mc sample as input for the sel eff plotting
    dsid_files = finder.createGridMCDictionary('GG_qqn1-rpvLF')

    # dsid_files = ['448149', '448105', '448193']
    # dsid_files= ['448149']

    ## ones with "bad" slopes
    # dsid_files= ['448054', '448053', '448066', '448086', '448114', '448122', '448154', '448178', '448189', '448210']

    # dsid_files = []
    # param_dicts = [{'mGluino': 2000, 'mChi0': 1650, 'tau': 10}, {'mGluino': 1800, 'mChi0': 50, 'tau': 1}, {'mGluino': 2200, 'mChi0': 1250, 'tau': 0.1}, {'mGluino': 2000, 'mChi0': 850, 'tau': 0.01}]
    # for params in param_dicts:
    #     dsid = finder.getDSIDFromParams(params)
    #     print(dsid)
    #     dsid_files.append(dsid)

    for dsid_key in dsid_files:
        # for mc_priod in ["mc", "mc16a", "mc16d", "mc16e"]:
        for mc_priod in ["mc"]:
        # for mc_priod in ["mc16e"]:
            # cfg_file = 'metadata.{}.{}.cfg'.format(mc_priod, dsid_key)
            # cfg_file = os.path.join(path, filename)
            cmd = "root -l -q -b \'macroSelectionEfficiency.cxx(\"{path}/{period}/metadata.{period}.{dsid}.cfg\")\' > {path}/{period}/logfile.plotSelectionEfficiency.{period}.{dsid}.log".format(path=path, period=mc_priod, dsid=dsid_key)
            print(cmd)
            # return
            sema.acquire()
            p = multiprocessing.Process(target=helper.callCmd, args=(cmd, dsid_key, sema))
            all_processes.append(p)
            p.start()

    for p in all_processes:
        p.join()

if __name__ == "__main__":
    main()
