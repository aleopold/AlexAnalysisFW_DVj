// clang-format off
/*
*
* root -l -q -b 'macroLumiNormYield("/eos/user/a/aleopold/jetdv/outputs/output_20220126_155731/plot_tmp/lumiNormYield.root")'
*/
// clang-format on

#include "PlottingUtils.h"
#include "PlottingStyle.h"
#include "Configurator.h"
#include "CommonHelpers.h"

void macroStandardPlot(TString filename, TString obj_name) {

  gROOT->SetStyle("ATLAS");

  auto file = TFile::Open(filename, "READ");

  auto object = (TObject)

  auto canvas = new TCanvas();

  eff1->Draw("SAME");
  eff2->Draw("SAME");

  Color_t textcolor = kBlack;

  AL::ATLAS_LABEL(0.2, 0.85, textcolor, 1.2);
  AL::myText(0.34, 0.85, textcolor, "Internal", 0.06);

  // AL::myText(0.34, 0.85, textcolor, "HGTD", 0.06);
  AL::myText(0.2, 0.76, textcolor, "Single pion, #LT#mu#GT=0", 0.045);

  // auto legend = new TLegend(0.55, 0.9, 0.9, 0.75);
  auto legend = new TLegend(0.25, 0.4, 0.8, 0.2);
  legend->SetTextFont(42);
  legend->SetFillStyle(0);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.04);

  legend->AddEntry(eff1, "Rel 20.20, r11671, 0.1 GeV < p_{T}^{true} < 5 GeV",
                   "lp");
  legend->AddEntry(eff2, "Rel 21.9, r13322, p_{T}^{true} = 1 GeV", "lp");

  legend->Draw("SAME");

  gPad->RedrawAxis();

  TString plot_name = "extensionEffComparison.pdf";
  canvas->Print(plot_name);
}
