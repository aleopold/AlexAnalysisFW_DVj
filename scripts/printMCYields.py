import ROOT
import os

def printOutYields(dsid, selection):
    pass

def main():
    top_config_filename = os.path.join(os.getenv('ANA_FW_PATH'), "topConfig.cfg")

    config = ROOT.TEnv(top_config_filename)

    dsid = 448149

    filename = '/eos/user/a/aleopold/jetdv/outputs/output_20220202_094701/{period}/output.{period}.448149.root'

    # file = ROOT.TFile.Open(filename, 'READ')

    # histoname = 'HighPTPre/h_passed_evnts'
    mc_periods = ['mc16a', 'mc16d', 'mc16e']
    selections = ['HighPTPre', 'HighPTVR', 'HighPTSR', 'TrklessPre', 'TrklessVR', 'TrklessSR']

    for selection in selections:
        # histoname = '{}/h_passed_evnts'.format(selection)
        # histo = file.Get(histoname)

        # yieldval = histo.GetBinContent(2)
        # print(selection, " yield=", yieldval)
    # histo_file_name = 'output.{dp}.{rn}.root'.format(dp=data_period, rn=run_number)
    # histo_file_path = os.path.join(rootfile_path, histo_file_name)

        yieldval = 0.0
        for period in mc_periods:
            file = ROOT.TFile.Open(filename.format(period=period), 'READ')
            histoname = '{}/h_passed_evnts'.format(selection)
            histo = file.Get(histoname)
            yieldval_period = histo.GetBinContent(2)
            yieldval += yieldval_period
            print(period, " yieldval_period=", yieldval_period)
            file.Close()
        print(selection, " yield=", yieldval)

if __name__ == "__main__":
    main()
