import simpleHelperFncs as hlpr

import json
import os
import math
import subprocess

import ROOT

ROOT.gROOT.SetStyle('ATLAS')

def getRunNumberBoundaries(period):
    if period == '2015':
        return 266904, 284484
    elif period == '2016':
        return 297730, 311481
    elif period == '2017':
        return 324320, 341649
    elif period == '2018':
        return 348197, 364485

def getRelativeLumiUncert(period):
    if period == '2015':
        return 2.0
    elif period == '2016':
        return 2.0
    elif period == '2017':
        return 2.0
    elif period == '2018':
        return 2.0

def readLumiFile(filename):
    lumi_dict = {}
    with open(filename) as file:
        for line_i, line in enumerate(file):
            line = line.strip()
            # print(line_i, line)
            ## skip first line with column names
            if (line_i == 0):
                continue
            ## skip last line with summed up values
            if (line.startswith('Total')):
                continue
            run_number = line.split(',')[0]
            lumi = line.split(',')[6]
            # print("Run number: ", run_number, "lumi: ", lumi)
            lumi_dict[run_number] = float(lumi)
    return lumi_dict


def checkRuns(config, period):
    """
    Check if for each of the runs in the lumi files I have a ntuple, or if there
    are ntuples for which I am missing the lumi data
    """
    lumi_filename = os.path.join(os.getenv('ANA_FW_PATH'), 'data/{period}.csv'.format(period=period))

    lumi_dict = readLumiFile(lumi_filename)

    data_version = config.GetValue("data_version", ".")
    json_name = 'data/dataNtupleFilesPerRunNumber.{version}.json'.format(version=data_version)
    run_dict = hlpr.loadJsonFile(os.path.join(os.getenv('ANA_FW_PATH'), json_name))

    ## check if each lumi block has ntuples
    for run_nbr in lumi_dict:
        if not run_nbr in run_dict.keys():
            print('No ntuple for RunNumber: ', run_nbr, '(period: ', period, ')')


    ## check if each ntuple has a lumi block
    min_rn, max_rn = getRunNumberBoundaries(period)
    for run_nbr in run_dict:
        if int(run_nbr)<min_rn or int(run_nbr)>max_rn:
            continue
        if not run_nbr in lumi_dict.keys():
            print("No Lumi info for RunNumber", run_nbr)

def plot(config, period, outfile, selection):

    # lumi_filename = os.path.join(os.getenv('ANA_FW_PATH'), 'data/{period}.csv'.format(period=period))
    lumi_filename = os.path.join(os.getenv('ANA_FW_PATH'), 'data/lumitable_{period}.csv'.format(period=period))
    print('reading lumi file', lumi_filename)
    lumi_dict = readLumiFile(lumi_filename)

    ## data15, data16, data17, data18, data
    data_period = 'data{}'.format(period[2:len(period)])
    print('data period', data_period)


    # canvas = ROOT.TCanvas('c1','c1',3000,600)

    n = len(lumi_dict.keys())
    histo = ROOT.TH1F('histo'+period+selection, ';RunNumber;Luminosity normalised yields', n, 0, n)

    rootfile_path = os.path.join(config.GetValue("current_output_path", "."), data_period)
    for i, run_number in enumerate(lumi_dict.keys()):

        ## name the bin
        histo.GetXaxis().SetBinLabel(i + 1, run_number);

        histo_file_name = 'output.{dp}.{rn}.root'.format(dp=data_period, rn=run_number)
        histo_file_path = os.path.join(rootfile_path, histo_file_name)
        # print('histogram file: ', histo_file_path)

        if not os.path.exists(histo_file_path):
            histo.SetBinContent(i + 1, -1)
            print('rootfile does not exist for RunNumber: ', run_number, '(period: ', period, ')')
            continue

        data_file = ROOT.TFile(histo_file_path, 'READ')
        if not data_file:
            histo.SetBinContent(i + 1, -2)
            print('rootfile cannot be opend for RunNumber: ', run_number, '(period: ', period, ')')
            continue
        # data_histo = data_file.Get["TH1F"]('Preselection/h_passed_evnts')
        data_histo = data_file.Get('{sel}/h_passed_evnts'.format(sel=selection))
        # data_histo = ROOT.TH2F(data_histo)
        if not data_histo:
            histo.SetBinContent(i + 1, -3)
            print('Histogram does not exist for for RunNumber: ', run_number, '(period: ', period, ')', 'with selection', selection)
            data_file.Close()
            continue

        passed_events = data_histo.GetBinContent(2)
        lumi = lumi_dict[run_number]
        # print('passed_events', passed_events)
        histo.SetBinContent(i + 1, passed_events/lumi)

        dfdl = passed_events / (lumi * lumi) ##ignore minus sign
        dl = lumi * getRelativeLumiUncert(period) / 100.

        dfdn = 1. / lumi
        dn2 = passed_events ##square already here

        error = math.sqrt(dfdn * dfdn * dn2 + dfdl * dfdl * dl * dl)
        histo.SetBinError(i + 1, error)

        data_file.Close()

    # histo.Draw()
    outfile.cd()
    histo.Write()


    # plot_path = os.path.join(config.GetValue("output_plot_path", "."), 'lumiNormedPlots')
    # plot_name = 'lumiNormedEvnts.{}.'.format(period)
    #
    # canvas.Print(os.path.join(plot_path, plot_name + 'pdf'))
    # canvas.Print(os.path.join(plot_path, plot_name + 'png'))





def main():
    """
    root -l -q -b 'macroLumiNormYield.cxx("/afs/cern.ch/work/a/aleopold/private/jetdv/AlexAnalysisFW_DVj/topConfig.cfg", "/eos/user/a/aleopold/jetdv/outputs/output_20220301_113653/plot_tmp/lumiNormYield.root")'
    """
    ROOT.gROOT.SetBatch()


    # return

    top_config_filename = os.path.join(os.getenv('ANA_FW_PATH'), "topConfig.cfg")

    config = ROOT.TEnv(top_config_filename)

    checkRuns(config, '2015')
    checkRuns(config, '2016')
    checkRuns(config, '2017')
    checkRuns(config, '2018')

    outpath = os.path.join(config.GetValue("current_output_path", "."), 'plot_tmp')
    outfilename = os.path.join(outpath, 'lumiNormYield.root')
    out_file = ROOT.TFile(outfilename, "RECREATE")

    plot(config, '2015', out_file, "any")
    plot(config, '2016', out_file, "any")
    plot(config, '2017', out_file, "any")
    plot(config, '2018', out_file, "any")

    plot(config, '2015', out_file, "HighPTPre")
    plot(config, '2016', out_file, "HighPTPre")
    plot(config, '2017', out_file, "HighPTPre")
    plot(config, '2018', out_file, "HighPTPre")

    plot(config, '2015', out_file, "HighPTPreNEW")
    plot(config, '2016', out_file, "HighPTPreNEW")
    plot(config, '2017', out_file, "HighPTPreNEW")
    plot(config, '2018', out_file, "HighPTPreNEW")

    plot(config, '2015', out_file, "HighPTVR")
    plot(config, '2016', out_file, "HighPTVR")
    plot(config, '2017', out_file, "HighPTVR")
    plot(config, '2018', out_file, "HighPTVR")

    plot(config, '2015', out_file, "HighPTSR")
    plot(config, '2016', out_file, "HighPTSR")
    plot(config, '2017', out_file, "HighPTSR")
    plot(config, '2018', out_file, "HighPTSR")

    plot(config, '2015', out_file, "TrklessPre")
    plot(config, '2016', out_file, "TrklessPre")
    plot(config, '2017', out_file, "TrklessPre")
    plot(config, '2018', out_file, "TrklessPre")

    plot(config, '2015', out_file, "TrklessPre")
    plot(config, '2016', out_file, "TrklessPre")
    plot(config, '2017', out_file, "TrklessPre")
    plot(config, '2018', out_file, "TrklessPre")


    plot(config, '2015', out_file, "HighPT_OCCUP")
    plot(config, '2016', out_file, "HighPT_OCCUP")
    plot(config, '2017', out_file, "HighPT_OCCUP")
    plot(config, '2018', out_file, "HighPT_OCCUP")


    plot(config, '2015', out_file, "noprescale")
    plot(config, '2016', out_file, "noprescale")
    plot(config, '2017', out_file, "noprescale")
    plot(config, '2018', out_file, "noprescale")

    out_file.Close()
    print("written file", outfilename)

    cmd = "root -l -q -b \'macroLumiNormYield.cxx(\"{cfg}\", \"{filename}\")\'".format(cfg=top_config_filename, filename=outfilename)
    print(cmd)
    # subprocess.call(cmd, shell=True)

if __name__ == "__main__":
    main()
