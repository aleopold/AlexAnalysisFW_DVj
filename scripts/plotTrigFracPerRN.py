import simpleHelperFncs as hlpr

import json
import os
import math
import subprocess

import ROOT

ROOT.gROOT.SetStyle('ATLAS')

def readLumiFile(filename):
    lumi_dict = {}
    with open(filename) as file:
        for line_i, line in enumerate(file):
            line = line.strip()
            # print(line_i, line)
            ## skip first line with column names
            if (line_i == 0):
                continue
            ## skip last line with summed up values
            if (line.startswith('Total')):
                continue
            run_number = line.split(',')[0]
            lumi = line.split(',')[6]
            # print("Run number: ", run_number, "lumi: ", lumi)
            lumi_dict[run_number] = float(lumi)
    return lumi_dict


def plot(config, period, outfile, selection):
    lumi_filename = os.path.join(os.getenv('ANA_FW_PATH'), 'data/lumitable_{period}.csv'.format(period=period))
    print('reading lumi file', lumi_filename)
    lumi_dict = readLumiFile(lumi_filename)

    ## data15, data16, data17, data18, data
    data_period = 'data{}'.format(period[2:len(period)])
    print('data period', data_period)

    rootfile_path = os.path.join(config.GetValue("current_output_path", "."), data_period)

    trigger_names = ["pass_EF_6j70",
"pass_HLT_2j220_j120",  "pass_HLT_2j250_j120",  "pass_HLT_2j275_j140","pass_HLT_4j100",  "pass_HLT_4j110",  "pass_HLT_4j120",  "pass_HLT_4j130",  "pass_HLT_5j100",  "pass_HLT_5j60",  "pass_HLT_5j65_0eta240_L14J150ETA25",  "pass_HLT_5j70",  "pass_HLT_5j75_0eta250",  "pass_HLT_5j85",  "pass_HLT_5j85_lcw",  "pass_HLT_5j90",  "pass_HLT_6j45",  "pass_HLT_6j45_0eta240",  "pass_HLT_6j55_0eta240_L14J150ETA25",  "pass_HLT_6j60",  "pass_HLT_6j70",  "pass_HLT_6j85",  "pass_HLT_7j45", "pass_HLT_7j50", "pass_HLT_g140_loose"]

    histo_dict = {}
    n = len(lumi_dict.keys())

    for trigger_name in trigger_names:


        histo = ROOT.TH1F('histo'+period+selection+trigger_name, ';RunNumber;Trigger fraction', n, 0, n)
        histo_dict[trigger_name] = histo


    for i, run_number in enumerate(lumi_dict.keys()):

        ## name the bin
        for trigger_name in trigger_names:
            histo_dict[trigger_name].GetXaxis().SetBinLabel(i + 1, run_number)

        histo_file_name = 'output.{dp}.{rn}.root'.format(dp=data_period, rn=run_number)
        histo_file_path = os.path.join(rootfile_path, histo_file_name)
        # print('histogram file: ', histo_file_path)

        if not os.path.exists(histo_file_path):
            for trigger_name in trigger_names:
                histo_dict[trigger_name].SetBinContent(i + 1, -1)
            print('rootfile does not exist for RunNumber: ', run_number, '(period: ', period, ')')
            continue

        data_file = ROOT.TFile(histo_file_path, 'READ')
        if not data_file:
            for trigger_name in trigger_names:
                histo_dict[trigger_name].SetBinContent(i + 1, -2)
            print('rootfile cannot be opend for RunNumber: ', run_number, '(period: ', period, ')')
            continue
        # data_histo = data_file.Get["TH1F"]('Preselection/h_passed_evnts')

        for trigger_name in trigger_names:

            data_histo = data_file.Get('{sel}/eff_{name}'.format(sel=selection, name = trigger_name))
            # data_histo = ROOT.TH2F(data_histo)
            if not data_histo:
                for trigger_name in trigger_names:
                    histo_dict[trigger_name].SetBinContent(i + 1, -3)
                print('Histogram does not exist for for RunNumber: ', run_number, '(period: ', period, ')')
                data_file.Close()
                continue

            eff = data_histo.GetEfficiency(1)
            eff_errlow = data_histo.GetEfficiencyErrorLow(1)
            eff_errhigh = data_histo.GetEfficiencyErrorUp(1)
            histo_dict[trigger_name].SetBinContent(i + 1, eff)
            histo_dict[trigger_name].SetBinError(i + 1, max(eff_errlow, eff_errhigh))

        data_file.Close()

    # histo.Draw()
    outfile.cd()
    for histo in histo_dict.values():
        histo.Write()




def main():
    ROOT.gROOT.SetBatch()

    top_config_filename = os.path.join(os.getenv('ANA_FW_PATH'), "topConfig.cfg")

    config = ROOT.TEnv(top_config_filename)

    outpath = os.path.join(config.GetValue("current_output_path", "."), 'plot_tmp')
    outfilename = os.path.join(outpath, 'trigger_rates.root')
    out_file = ROOT.TFile(outfilename, "RECREATE")

    plot(config, '2015', out_file, "HighPTPre")
    plot(config, '2016', out_file, "HighPTPre")
    plot(config, '2017', out_file, "HighPTPre")
    plot(config, '2018', out_file, "HighPTPre")

    out_file.Close()
    print("written file", outfilename)
    cmd = "root -l -q -b \'macroTrigFracPerRN.cxx(\"{cfg}\", \"{filename}\")\'".format(cfg=top_config_filename, filename=outfilename)
    print(cmd)


if __name__ == "__main__":
    main()
