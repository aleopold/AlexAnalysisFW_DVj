/**
 * @author Alexander Leopold (alexander.leopold@cern.ch)
 * @brief  Defines helper classes to book objects beyond THn
 */

class TEffBooker : public ROOT::Detail::RDF::RActionImpl<TEffBooker> {
public:

  TEffBooker(std::string_view name, std::string_view title, int nbins,
             double xlow, double xup) {
    const auto nSlots =
        ROOT::IsImplicitMTEnabled() ? ROOT::GetThreadPoolSize() : 1;

    for (auto i : ROOT::TSeqU(nSlots)) {
      m_efficiencies.emplace_back(std::make_shared<TEfficiency>(
          std::string(name).c_str(), std::string(title).c_str(), nbins, xlow,
          xup));
      (void)i;
    }
  }

  TEffBooker(std::string_view name, std::string_view title, int nbins,
             const double* xbins) {
    const auto nSlots =
        ROOT::IsImplicitMTEnabled() ? ROOT::GetThreadPoolSize() : 1;

    for (auto i : ROOT::TSeqU(nSlots)) {
      m_efficiencies.emplace_back(std::make_shared<TEfficiency>(
          std::string(name).c_str(), std::string(title).c_str(), nbins, xbins));
      (void)i;
    }
  }

  TEffBooker(TEffBooker&&) = default;
  TEffBooker(const TEffBooker&) = delete;

  /// This type is a requirement for every helper.
  using Result_t = TEfficiency;

private:
  // one per data processing slot
  std::vector<std::shared_ptr<TEfficiency>> m_efficiencies;

public:
  std::shared_ptr<TEfficiency> GetResultPtr() const {
    return m_efficiencies[0];
  }
  void Initialize() {}
  void InitTask(TTreeReader*, unsigned int) {}

  void Exec(unsigned int slot, bool passes, float value, float weight = 1.) {

    m_efficiencies[slot]->FillWeighted(passes, weight, value);
    // m_efficiencies[slot]->Fill(passes, value);
  }

  /// This method is called at the end of the event loop. It is used to merge
  /// all the internal THnTs which were used in each of the data processing
  /// slots.
  void Finalize() {
    auto& res = m_efficiencies[0];
    for (auto slot : ROOT::TSeqU(1, m_efficiencies.size())) {
      res->Add(*m_efficiencies[slot].get());
    }
  }

  std::string GetActionName() { return "TEffBooker"; }
};
