

using SelectionDF_t =
    ROOT::RDF::RInterface<ROOT::Detail::RDF::RLoopManager, void>;
using FilteredDF_t =
    ROOT::RDF::RInterface<ROOT::Detail::RDF::RJittedFilter, void>;

using FourVec_t = ROOT::Math::PtEtaPhiMVector;
using RVecB_t = ROOT::VecOps::RVec<bool>;
using RVecC_t = ROOT::VecOps::RVec<char>;
using RVecD_t = ROOT::VecOps::RVec<double>;
using RVecF_t = ROOT::VecOps::RVec<float>;
using RVecI_t = ROOT::VecOps::RVec<int>;
using RVecUL_t = ROOT::VecOps::RVec<unsigned long>;

auto defineAdditionalVariables(ROOT::RDataFrame& dataframe, bool is_data,
                               float weight_scale, bool blinded = true) {

  auto leadingJet = [](RVecD_t pt, RVecD_t eta, RVecD_t phi, RVecD_t m) {
    FourVec_t leading_jet = FourVec_t(0, 0, 0, 0);
    for (size_t i = 0; i < pt.size(); i++) {
      auto buff_4v = FourVec_t(pt.at(i), eta.at(i), phi.at(i), m.at(i));
      if (buff_4v.Pt() > leading_jet.Pt()) {
        leading_jet = buff_4v;
      }
    }
    return leading_jet;
  };

  auto leadingJetPt = [](FourVec_t four_vec) { return four_vec.Pt(); };
  auto leadingJetPhi = [](FourVec_t four_vec) { return four_vec.Phi(); };
  auto leadingJetEta = [](FourVec_t four_vec) { return four_vec.Eta(); };

  std::vector<std::string> evnt_weight_columns = {"mcEventWeight"};
  if (is_data) {
    // in this case the weight is 1.
    // still need a dummy branch of type float for the Define to work with mc &
    // data input
    evnt_weight_columns = {"averageInteractionsPerCrossing"};
  }
  auto eventWeight = [is_data, weight_scale](float evnt_weight) {
    return is_data ? 1. : evnt_weight * weight_scale;
  };

  std::string dv_selection_str =
      "DV_index[DV_passFiducialCut ==1 && "
      "DV_passChiSqCut==1 && DV_passDistCut==1 && "
      "DV_passMaterialVeto_strict==1 && DV_passNTrkCut==1 "
      "&& DV_passMassCut==1]";
  if (is_data and blinded) {
    dv_selection_str = "DV_index[DV_passFiducialCut ==1 && "
                       "DV_passChiSqCut==1 && DV_passDistCut==1 && "
                       "DV_passMaterialVeto_strict==1 && DV_nTracks<=4"
                       " && DV_m<10.]";
  }

  auto sel_dv_value_double = [](RVecUL_t sel_dv_index, RVecUL_t all_dv_index,
                                RVecD_t all_dv_r) {
    RVecF_t dv_r;

    for (size_t i = 0; i < all_dv_index.size(); i++) {
      for (size_t j = 0; j < sel_dv_index.size(); j++) {
        if (all_dv_index.at(i) == sel_dv_index.at(j)) {
          dv_r.push_back(all_dv_r.at(i));
        }
      }
    }
    return dv_r;
  };

  auto sel_dv_value_float = [](RVecUL_t sel_dv_index, RVecUL_t all_dv_index,
                               RVecF_t all_dv_r) {
    RVecF_t dv_r;

    for (size_t i = 0; i < all_dv_index.size(); i++) {
      for (size_t j = 0; j < sel_dv_index.size(); j++) {
        if (all_dv_index.at(i) == sel_dv_index.at(j)) {
          dv_r.push_back(all_dv_r.at(i));
        }
      }
    }
    return dv_r;
  };

  //   "( \
  // Sum$(calibJet_Pt > 250. && calibJet_isFlaggedBad == 0 && calibJet_sPassOR == 1) >= 4 || \
  // Sum$(calibJet_Pt > 195. && calibJet_isFlaggedBad == 0 && calibJet_sPassOR == 1) >= 5 || \
  // Sum$(calibJet_Pt > 116.  && calibJet_isFlaggedBad == 0 && calibJet_sPassOR == 1) >= 6 || \
  // Sum$(calibJet_Pt > 90.  && calibJet_isFlaggedBad == 0 && calibJet_sPassOR == 1) >= 7 \
  // )"

  // calibJet_isFlaggedBad : vector<char>
  // calibJet_sPassOR : vector<int>
  auto jetSelectionHighPt = [](RVecD_t pt, RVecD_t eta, RVecD_t phi, RVecD_t m,
                               RVecC_t flagged_bad, RVecI_t sPassOR) {
    // get the good jets
    std::vector<FourVec_t> jets;
    for (size_t i = 0; i < pt.size(); i++) {
      if (flagged_bad.at(i) != 0 or sPassOR.at(i) != 1) {
        continue;
      }
      auto buff_4v = FourVec_t(pt.at(i), eta.at(i), phi.at(i), m.at(i));
      jets.push_back(buff_4v);
    }
    // sort them
    // std::sort(
    //     std::begin(jets), std::end(jets),
    //     [](const FourVec_t& a, const FourVec_t& b) { return a.Pt() > b.Pt();
    //     });

    int n1 = 0;
    int n2 = 0;
    int n3 = 0;
    int n4 = 0;
    for (const auto& jet : jets) {
      if (jet.Pt() > 250.) {
        n1++;
      }
      if (jet.Pt() > 195.) {
        n2++;
      }
      if (jet.Pt() > 116.) {
        n3++;
      }
      if (jet.Pt() > 90.) {
        n4++;
      }
    }

    return n1 >= 4 or n2 >= 5 or n3 >= 6 or n4 >= 7;
  };

  //   Sum$(calibJet_Pt > 137. && calibJet_isFlaggedBad == 0 && calibJet_sPassOR == 1) >= 4 || \
  // Sum$(calibJet_Pt > 101. && calibJet_isFlaggedBad == 0 && calibJet_sPassOR == 1) >= 5 || \
  // Sum$(calibJet_Pt > 83.  && calibJet_isFlaggedBad == 0 && calibJet_sPassOR == 1) >= 6 || \
  // Sum$(calibJet_Pt > 55.  && calibJet_isFlaggedBad == 0 && calibJet_sPassOR == 1) >= 7 \
  // ) && (\
  // Sum$(calibJet_Pt > 78. && fabs(calibJet_Eta) < 2.5 && SumPtTrkPt500 < 5. && calibJet_isFlaggedBad == 0 && calibJet_sPassOR == 1) >= 1 || \
  // Sum$(calibJet_Pt > 56. && fabs(calibJet_Eta) < 2.5 && SumPtTrkPt500 < 5. && calibJet_isFlaggedBad == 0 && calibJet_sPassOR == 1) >= 2 \
  // ) && \
  // !
  // SumPtTrkPt500 : vector<double>

  auto jetSelectionTrackless =
      [](RVecD_t pt, RVecD_t eta, RVecD_t phi, RVecD_t m, RVecC_t flagged_bad,
         RVecI_t sPassOR, RVecD_t sumpt_trk, bool passes_highpt) {
        // get the good jets
        std::vector<FourVec_t> jets;
        std::vector<double> used_sumpttrk;
        for (size_t i = 0; i < pt.size(); i++) {
          if (flagged_bad.at(i) != 0 or sPassOR.at(i) != 1) {
            continue;
          }
          auto buff_4v = FourVec_t(pt.at(i), eta.at(i), phi.at(i), m.at(i));
          jets.push_back(buff_4v);
          used_sumpttrk.push_back(sumpt_trk.at(i));
        }

        int n1 = 0;
        int n2 = 0;
        int n3 = 0;
        int n4 = 0;
        int n5 = 0;
        int ntless1 = 0;
        int ntless2 = 0;

        for (size_t i = 0; i < jets.size(); i++) {
          if (jets.at(i).Pt() > 137.) {
            n1++;
          }
          if (jets.at(i).Pt() > 101.) {
            n2++;
          }
          if (jets.at(i).Pt() > 83.) {
            n3++;
          }
          if (jets.at(i).Pt() > 55.) {
            n5++;
          }
          if (jets.at(i).Pt() > 78. and std::abs(jets.at(i).Eta()) < 2.5 and
              used_sumpttrk.at(i) < 5.) {
            ntless1++;
          }
          if (jets.at(i).Pt() > 56. and std::abs(jets.at(i).Eta()) < 2.5 and
              used_sumpttrk.at(i) < 5.) {
            ntless2++;
          }
        }

        return (n1 >= 4 or n2 >= 5 or n3 >= 6 or n4 >= 7) and
               (ntless1 >= 1 or ntless2 >= 2) and not passes_highpt;
      };

  // of type ROOT::RDF::RInterface<ROOT::Detail::RDF::RLoopManager, void>&

  // selection "DRAW_pass_triggerFlags==1 && (BaselineSel_HighPtSR==1 ||
  // (BaselineSel_DoubleTrackless==1 || BaselineSel_SingleTrackless==1)) &&
  // DRAW_pass_DVJETS==1"
  auto return_dataframe =
      dataframe
          .Define("passes_any", "DRAW_pass_DVJETS && (BaselineSel_HighPtSR==1 "
                                "|| (BaselineSel_DoubleTrackless==1 || "
                                "BaselineSel_SingleTrackless==1))")
          .Define("passes_HighPTPre", "BaselineSel_HighPtSR==1")
          .Define("passes_HighPTPreNEW", jetSelectionHighPt,
                  {"calibJet_Pt", "calibJet_Eta", "calibJet_Phi", "calibJet_M",
                   "calibJet_isFlaggedBad", "calibJet_sPassOR"})
          .Define("passes_TrklessPre", "BaselineSel_TracklessSR==1 ")
          .Define("passes_TrklessPreNEW", jetSelectionTrackless,
                  {"calibJet_Pt", "calibJet_Eta", "calibJet_Phi", "calibJet_M",
                   "calibJet_isFlaggedBad", "calibJet_sPassOR", "SumPtTrkPt500",
                   "passes_HighPTPreNEW"})
          .Define("sr_dv_idx", dv_selection_str)
          .Define("sr_blinded_mass_dv_idx",
                  "DV_index[DV_passFiducialCut ==1 && "
                  "DV_passChiSqCut==1 && DV_passDistCut==1 && "
                  "DV_passMaterialVeto_strict==1 && DV_passNTrkCut==1 "
                  "&& DV_passMassCut==0]")
          .Define("occupancy_sel_idx",
                  "DV_index[DV_passFiducialCut ==1 && DV_passChiSqCut==1 && "
                  "DV_passDistCut==1 && DV_passMaterialVeto_strict==1 && "
                  "DV_passNTrkCut==0 && DV_passMassCut==0]")
          .Define("occup_sel_dv_r", sel_dv_value_double,
                  {"occupancy_sel_idx", "DV_index", "DV_rxy"})
          .Define("occup_sel_dv_x", sel_dv_value_float,
                  {"occupancy_sel_idx", "DV_index", "DV_x"})
          .Define("occup_sel_dv_y", sel_dv_value_float,
                  {"occupancy_sel_idx", "DV_index", "DV_y"})
          .Define("occup_sel_dv_z", sel_dv_value_float,
                  {"occupancy_sel_idx", "DV_index", "DV_z"})
          .Define("occup_sel_dv_mass", sel_dv_value_double,
                  {"occupancy_sel_idx", "DV_index", "DV_m"})
          .Define("occup_sel_dv_myr", "sqrt(occup_sel_dv_x*occup_sel_dv_x + "
                                      "occup_sel_dv_y*occup_sel_dv_y)")
          .Define("passes_SR", "sr_dv_idx.size()>=1")
          .Define("passes_HighPTSR", "passes_HighPTPre && passes_SR")
          .Define("passes_TrklessSR", "passes_TrklessPre && passes_SR")
          .Define("passes_HighPTSR_NEW", "passes_HighPTPreNEW && passes_SR")
          .Define("passes_TrklessSR_NEW", "passes_TrklessPreNEW && passes_SR")
          .Define("passes_SR_BLINDED", "sr_blinded_mass_dv_idx.size()>=1")
          .Define("passes_HighPTSR_BLINDED",
                  "passes_HighPTPre && passes_SR_BLINDED")
          .Define("passes_TrklessSR_BLINDED",
                  "passes_TrklessPre && passes_SR_BLINDED")
          .Define("passes_HighPT_OCCUP",
                  "passes_HighPTPre && occupancy_sel_idx.size()>=1")
          .Define("passes_Trkless_OCCUP",
                  "passes_TrklessPre && occupancy_sel_idx.size()>=1")
          .Define("passes_HighPTNEW_OCCUP",
                  "passes_HighPTPreNEW && occupancy_sel_idx.size()>=1")
          .Define("passes_TrklessNEW_OCCUP",
                  "passes_TrklessPreNEW && occupancy_sel_idx.size()>=1")
          // NEW Dffs
          .Define("passes_HighPTSR_NEW_BLINDED",
                  "passes_HighPTPreNEW && passes_SR_BLINDED")
          .Define("passes_TrklessSR_NEW_BLINDED",
                  "passes_TrklessPreNEW && passes_SR_BLINDED")
          .Define("passes_HighPT_NEW_OCCUP",
                  "passes_HighPTPreNEW && occupancy_sel_idx.size()>=1")
          .Define("passes_Trkless_NEW_OCCUP",
                  "passes_TrklessPreNEW && occupancy_sel_idx.size()>=1")
          .Define("vr_dv_idx", "DV_index[DV_passFiducialCut ==1 && "
                               "DV_passChiSqCut==1 && DV_passDistCut==1 && "
                               "DV_passMaterialVeto==0 && DV_passNTrkCut==1 "
                               "&& DV_passMassCut==1]")
          .Define("passes_VR", "vr_dv_idx.size()>=1")
          .Define("passes_HighPTVR", "passes_HighPTPre && passes_VR")
          .Define("passes_TrklessVR", "passes_TrklessPre && passes_VR")
          .Define("event_weight", eventWeight, evnt_weight_columns)
          .Define("leading_jet", leadingJet,
                  {"calibJet_Pt", "calibJet_Eta", "calibJet_Phi", "calibJet_M"})
          .Define("leading_jet_pt", leadingJetPt, {"leading_jet"})
          .Define("leading_jet_phi", leadingJetPhi, {"leading_jet"})
          .Define("leading_jet_eta", leadingJetEta, {"leading_jet"})
          .Define("n_trigger_decisions", "TriggerDecisions.size()")
          .Define("zeroP5", "0.5");

  return return_dataframe;
}

auto getPreselectionFrame(SelectionDF_t& df_with_defines,
                          const std::string& presel_name) {
  auto selected_frame = df_with_defines.Filter(presel_name);
  return selected_frame;
}

auto getValidationRegionFrame(SelectionDF_t& df_with_defines,
                              const std::string& presel_name) {
  auto selected_frame = df_with_defines.Filter(presel_name).Filter("passes_VR");

  return selected_frame;
}

auto getSignalRegionFrame(SelectionDF_t& df_with_defines,
                          const std::string& presel_name, bool blinded = true) {
  std::string_view selection = blinded ? "passes_SR_BLINDED" : "passes_SR";

  auto selected_frame = df_with_defines.Filter(presel_name).Filter(selection);
  return selected_frame;
}
