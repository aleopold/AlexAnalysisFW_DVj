#include "BookHelpers.cxx"

using SelectionDF_t =
    ROOT::RDF::RInterface<ROOT::Detail::RDF::RLoopManager, void>;
using FilteredDF_t =
    ROOT::RDF::RInterface<ROOT::Detail::RDF::RJittedFilter, void>;
using TH1ResPtr_t = ROOT::RDF::RResultPtr<TH1D>;
using TH2ResPtr_t = ROOT::RDF::RResultPtr<TH2D>;
using TEffResPtr_t = ROOT::RDF::RResultPtr<TEfficiency>;

struct HistOutputStorage {
  using StrType_t = std::string;
  // name of selection / all defined histograms
  std::map<StrType_t, std::vector<TH1ResPtr_t>> m_h_results;
  std::map<StrType_t, std::vector<TH2ResPtr_t>> m_h2_results;
  std::map<StrType_t, std::vector<TEffResPtr_t>> m_teff_results;
  std::map<StrType_t, TDirectory*> m_dirs;

  void addH1(TH1ResPtr_t h, const StrType_t& key) {
    m_h_results[key].push_back(h);
  }

  void addH2(TH2ResPtr_t h, const StrType_t& key) {
    m_h2_results[key].push_back(h);
  }

  void addEff(TEffResPtr_t h, const StrType_t& key) {
    m_teff_results[key].push_back(h);
  }

  void writeToFile(const StrType_t& filename) {
    TFile* output_file = TFile::Open(filename.c_str(), "RECREATE");
    for (auto& [name, objects] : m_h_results) {
      // create new directory for specific selection
      TDirectory* dir = output_file->mkdir(name.c_str());
      // add it to the map for later access
      m_dirs[name] = dir;
      dir->cd();
      for (auto& obj : objects) {
        // obj->SetName((name + "/" + obj->GetName()).c_str());
        obj->Write();
      }
    }
    for (auto& [name, objects] : m_h2_results) {
      m_dirs[name]->cd();
      for (auto& obj : objects) {
        obj->Write();
      }
    }
    for (auto& [name, objects] : m_teff_results) {
      m_dirs[name]->cd();
      for (auto& obj : objects) {
        obj->Write();
      }
    }
    output_file->Close();
  }
};

void scheduleH1s(FilteredDF_t& sel_frame, HistOutputStorage& storage,
                 const std::string& sel_name) {
  // auto h = sel_frame.Histo1D({}, {});
  // storage.addH1(sel_frame.Histo1D({cmbNms(sel_name, "/h_dv_r"),
  //                                  ";#DV radius [mm];Entries", 100, 0, 30},
  //                                 "DV_rxy"),
  //               sel_name);
  storage.addH1(
      sel_frame.Histo1D({"h_dv_r", ";DV radius [mm];Entries", 100, 0, 500},
                        "DV_rxy", "event_weight"),
      sel_name);
  storage.addH1(
      sel_frame.Histo1D({"h_sel_dv_r", ";DV radius [mm];Entries", 100, 0, 500},
                        "occup_sel_dv_r", "event_weight"),
      sel_name);
  storage.addH1(
      sel_frame.Histo1D({"h_sel_dv_mass", ";DV mass [GeV];Entries", 100, 0, 20},
                        "occup_sel_dv_mass", "event_weight"),
      sel_name);
  storage.addH1(sel_frame.Histo1D(
                    {"h_evnt_weight", ";Event weight;Entries", 100, -100, 100},
                    "event_weight"),
                sel_name);
  storage.addH1(
      sel_frame.Histo1D({"h_dv_n", ";Number of DVs;Entries", 30, 0, 30}, "DV_n",
                        "event_weight"),
      sel_name);
  std::string region_name = "passes_" + sel_name;
  storage.addH1(
      sel_frame.Histo1D({"h_passed_evnts", ";Passed;Entries", 2, 0, 2},
                        region_name, "event_weight"),
      sel_name);
  storage.addH1(
      sel_frame.Histo1D({"h_leadj_pt", ";p_{T}^{jet};Entries", 200, 0, 2800},
                        "leading_jet_pt", "event_weight"),
      sel_name);
  storage.addH1(
      sel_frame.Histo1D({"h_leadj_phi", ";#phi^{jet};Entries", 50, -3.5, 3.5},
                        "leading_jet_phi", "event_weight"),
      sel_name);
  storage.addH1(
      sel_frame.Histo1D({"h_leadj_eta", ";#eta^{jet};Entries", 80, -3.0, 3.0},
                        "leading_jet_eta", "event_weight"),
      sel_name);
  storage.addH1(
      sel_frame.Histo1D(
          {"h_avg_int", ";Average interactions per BC;Entries", 80, 0, 80},
          "averageInteractionsPerCrossing", "event_weight"),
      sel_name);
  storage.addH1(
      sel_frame.Histo1D(
          {"h_act_int", ";Actual interactions per BC;Entries", 80, 0, 80},
          "actualInteractionsPerCrossing", "event_weight"),
      sel_name);
  storage.addH1(
      sel_frame.Histo1D({"h_pv_sumpt2",
                         ";#sum p_{T}^{2} of the primary vertex;Entries", 100,
                         0, 10000},
                        "PV_sumpT2", "event_weight"),
      sel_name);
  storage.addH1(
      sel_frame.Histo1D(
          {"h_npv", ";Number of primary vertices;Entries", 50, 0, 100}, "NPV",
          "event_weight"),
      sel_name);
  storage.addH1(
      sel_frame.Histo1D({"h_n_trigger_decisions",
                         ";Number of stored triggers;Entries", 50, 0, 50},
                        "n_trigger_decisions", "event_weight"),
      sel_name);
}

void scheduleH2s(FilteredDF_t& sel_frame, HistOutputStorage& storage,
                 const std::string& sel_name) {
  // auto h = sel_frame.Histo1D({}, {});
  storage.addH2(sel_frame.Histo2D({"h2_leadjet_occup",
                                   ";#eta_{leading jet};#phi_{leading jet}", 30,
                                   -3.0, 3.0, 70, -3.5, 3.5},
                                  "leading_jet_eta", "leading_jet_phi",
                                  "event_weight"),
                sel_name);

  storage.addH2(sel_frame.Histo2D({"h2_sel_dv_zr", ";z [mm]; Radius [mm]", 100,
                                   -300, 300, 100, 0, 300},
                                  "occup_sel_dv_z", "occup_sel_dv_r",
                                  "event_weight"),
                sel_name);
  storage.addH2(sel_frame.Histo2D({"h2_sel_dv_zx", ";z [mm]; x [mm]", 100, -300,
                                   300, 100, -300, 300},
                                  "occup_sel_dv_z", "occup_sel_dv_x",
                                  "event_weight"),
                sel_name);
  storage.addH2(sel_frame.Histo2D({"h2_sel_dv_yz", ";z [mm]; y [mm]", 100, -300,
                                   300, 100, -300, 300},
                                  "occup_sel_dv_z", "occup_sel_dv_y",
                                  "event_weight"),
                sel_name);
  storage.addH2(sel_frame.Histo2D({"h2_sel_dv_xy", ";x [mm]; y [mm]", 100, -300,
                                   300, 100, -300, 300},
                                  "occup_sel_dv_x", "occup_sel_dv_y",
                                  "event_weight"),
                sel_name);
}

void scheduleEffs(SelectionDF_t& sel_frame, HistOutputStorage& storage,
                  const std::string& sel_name, bool blind_sr) {
  std::string pass_region_name = "passes_" + sel_name;
  storage.addEff(
      sel_frame.Book<bool, float, double>(
          TEffBooker{
              "eff_sel_vs_avrg_int",
              "Event selection efficiency;Average interactions per crossing",
              80, 0, 80},
          {pass_region_name, "averageInteractionsPerCrossing", "event_weight"}),
      sel_name);
  storage.addEff(
      sel_frame.Book<bool, float, double>(
          TEffBooker{
              "eff_sel_vs_act_int",
              "Event selection efficiency;Average interactions per crossing ",
              80, 0, 80},
          {pass_region_name, "actualInteractionsPerCrossing", "event_weight"}),
      sel_name);

  std::vector<std::string> trigger_names_bool = {
      "pass_EF_6j70",
      "pass_HLT_2j220_j120",
      "pass_HLT_2j250_j120",
      "pass_HLT_2j275_j140",
      "pass_HLT_4j100",
      "pass_HLT_4j110",
      "pass_HLT_4j120",
      "pass_HLT_4j130",
      "pass_HLT_5j100",
      "pass_HLT_5j60",
      "pass_HLT_5j65_0eta240_L14J150ETA25",
      "pass_HLT_5j70",
      "pass_HLT_5j75_0eta250",
      "pass_HLT_5j85",
      "pass_HLT_5j85_lcw",
      "pass_HLT_5j90",
      "pass_HLT_6j45",
      "pass_HLT_6j45_0eta240",
      "pass_HLT_6j55_0eta240_L14J150ETA25",
      "pass_HLT_6j60",
      "pass_HLT_6j70",
      "pass_HLT_6j85",
      "pass_HLT_7j45",
      "pass_HLT_7j50"};
  // "pass_HLT_j70_j50_0eta490_invm1100j70_dphi20_deta40_L1MJJ-500-NFF" doesn't
  // exist?
  std::string trg_eff_name = "eff_";

  for (size_t trg_i = 0; trg_i < trigger_names_bool.size(); trg_i++) {

    storage.addEff(
        sel_frame.Book<int, double, double>(
            TEffBooker{trg_eff_name + trigger_names_bool.at(trg_i),
                       ";Trigger rate;", 1, 0, 1},
            {trigger_names_bool.at(trg_i), "zeroP5", "event_weight"}),
        sel_name);
  }

  std::vector<std::string> trigger_names_int = {"pass_HLT_g140_loose"};
  for (size_t trg_i = 0; trg_i < trigger_names_int.size(); trg_i++) {
    storage.addEff(sel_frame.Book<bool, double, double>(
                       TEffBooker{trg_eff_name + trigger_names_int.at(trg_i),
                                  ";Trigger rate;", 1, 0, 1},
                       {trigger_names_int.at(trg_i), "zeroP5", "event_weight"}),
                   sel_name);
  }

  // if (not blind_sr) {
  // TODO this does not work with vectors
  //   storage.addEff(
  //       sel_frame.Book<bool, float, double>(
  //           TEffBooker{"eff_sel_vs_dvr",
  //                      "Event selection efficiency;DV radius [mm]", 100, 0,
  //                      500},
  //           {pass_region_name, "DV_rxy", "event_weight"}),
  //       sel_name);
  // }
}
