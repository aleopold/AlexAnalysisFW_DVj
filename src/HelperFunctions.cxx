#include "CommonHelpers.h"

#include <string>

float getSumOfWeights(const std::vector<TString>& filenames) {
  float sow = 0.;
  for (const auto& filename : filenames) {
    auto file = TFile::Open(filename, "READ");
    // retrieve sumofweight from the MetaData_EventCount histo
    auto histo = file->Get<TH1D>("MetaData_EventCount");
    // the sum of weights are stored in bin 4
    sow += histo->GetBinContent(4);
    std::cout << "[getSumOfWeights] INFO reading=" << histo->GetBinContent(4)
              << " from " << filename << '\n';
    file->Close();
  }
  std::cout << "[getSumOfWeights] INFO returning SOW=" << sow << '\n';
  return sow;
}

void cleanBadFiles(std::vector<TString>& strings) {
  std::vector<TString> badstrings = {"group.phys-susy.00305543.r11969_r11784_"
                                     "p4072_p4296.26635556._000224.trees.root",
                                     "group.phys-susy.00309390.r11969_r11784_"
                                     "p4072_p4296.26635556._000279.trees.root",
                                     "group.phys-susy.00310370.r11969_r11784_"
                                     "p4072_p4296.26635556._000118.trees.root",
                                     "group.phys-susy.00299584.r11969_r11784_"
                                     "p4072_p4296.26635556._000344.trees.root"};

  for (size_t i = 0; i < badstrings.size(); i++) {
    strings.erase(std::remove_if(strings.begin(), strings.end(),
                                 [badstrings, i](const TString& str) {
                                   return str.Contains(badstrings.at(i));
                                 }),
                  strings.end());
  }
}

std::map<int, double> readLuminosityData(const TString& path_to_lumi_files,
                                         const TString& csv_name) {
  // read in files that store the data like this:
  // Run, Good, Bad, LDelivered, LRecorded, LAr Corrected, Prescale Corrected,
  // Live Fraction, LAr Fraction, Prescale Fraction 348885, 502, 0, 107.985176,
  // 104.344666, 104.338741, 104.338741, 96.63, 99.99, 100.00 348894, 13,
  // 0, 3.391681, 3.252448, 3.252389, 3.252389, 95.89, 100.00, 100.00

  // TString path_to_lumi_files =
  // "/home/aleopold/jet_plus_dv/analysis_fw/data/";

  std::map<int, double> lumi_map;

  ifstream csv_file;
  TString full_path = path_to_lumi_files + csv_name;
  csv_file.open(full_path.Data());
  if (not csv_file.is_open()) {
    throw std::runtime_error("readLuminosityData: ERROR, wrong path!");
  }

  std::string line;

  std::getline(csv_file, line); // skip the 1st line
  while (std::getline(csv_file, line)) {
    if (line.empty()) {
      continue; // skip emtpy lines
    }

    vector<TString> line_vec = AL::vectorize(TString(line), ",");
    if (line_vec.at(0) == "Total") {
      continue;
    }
    int run_number = line_vec.at(0).Atoi();
    double luminosity = line_vec.at(6).Atof();
    lumi_map.insert(std::pair<int, double>(run_number, luminosity));
  }
  csv_file.close();

  return lumi_map;
}
